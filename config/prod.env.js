'use strict'
module.exports = {
  NODE_ENV: '"production"',
  ENV_CONFIG: '"prod"',
  DEBUG: false,
  ORDER_SETTING: '""',
  DOMAIN_CTX:'"http://app.lezhide.cn/"',
  CTX:'"http://api.lezhide.cn/"',
  FILE_CTX:'"http://oss.lezhide.cn/"',
  WS_CTX:'"ws.lezhide.cn/"',
}

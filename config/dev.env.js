'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  ENV_CONFIG: '"dev"',
  DEBUG: true,
  ORDER_SETTING: '""',
  DOMAIN_CTX: '"http://tapp.lezhide.cn/"',
  // CTX:'"http://192.168.0.254:7002/"',
  // WS_CTX:'"192.168.0.254:7005/"',
  // FILE_CTX:'"http://dev-oss.lezhide.cn/"',
  // CTX:'"http://192.168.0.66:7002/"',
  // WS_CTX:'"192.168.0.66:7005/"',

  CTX: '"http://tapi.lezhide.cn/"',
  WS_CTX: '"ws.lezhide.cn/"',
  FILE_CTX: '"http://test-oss.lezhide.cn/"'
})

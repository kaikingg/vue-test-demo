/**
 * 正常业务
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */

// const Persional = () => import('@/views/common/persional/index')
// const MyGrowPath = () => import('@/views/common/persional/my-grow-path.vue')
// const PersionalChild = () => import('@/views/common/persional/persional-child.vue')
// const MemberManage = () => import('@/views/common/persional/member-manage.vue')
// const MemberInvitation = () => import('@/views/common/persional/member-invitation.vue')

export default [
  {
    path: '/common/persional/index', name: 'Persional',
    component: resolve => require(['@/views/common/persional/index'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '档案信息'
      }
    }
  },
  {
    path: '/common/persional/my-grow-path', name: 'MyGrowPath',
    component: resolve => require(['@/views/common/persional/my-grow-path.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '成长印记'
      }
    }
  },
  {
    path: '/common/persional/persional-child', name: 'PersionalChild',
    component: resolve => require(['@/views/common/persional/persional-child.vue'], resolve),
    meta: {
      index: 2
    }
  },
  {
    path: '/common/persional/member-manage', name: 'MemberManage',
    component: resolve => require(['@/views/common/persional/member-manage.vue'], resolve),
    meta: {
      index: 2
    }
  },
  {
    path: '/common/persional/member-invitation', name: 'MemberInvitation',
    component: resolve => require(['@/views/common/persional/member-invitation.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '邀请成员'
      }
    }
  },
  {
    path: '/common/persional/help-center', name: 'HelpCenter',
    component: resolve => require(['@/views/common/persional/help-center.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '帮助中心'
      }
    }
  },
  {
    path: '/common/persional/all-question', name: 'AllQuestion',
    component: resolve => require(['@/views/common/persional/all-question.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '全部问题'
      }
    }
  },
  {
    path: '/common/persional/setting', name: 'setting',
    component: resolve => require(['@/views/common/persional/setting.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '系统设置'
      }
    }
  },
  {
    path: '/common/persional/about-campus-assistant', name: 'about',
    component: resolve => require(['@/views/common/persional/about.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '关于校园助手'
      }
    }
  },
  {
    path: '/common/persional/about-us', name: 'AboutUs',
    component: resolve => require(['@/views/common/persional/about-us.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '关于校园助手'
      }
    }
  }
]

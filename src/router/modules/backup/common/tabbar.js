/**
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */
export default[
  {
    path: '/', name: 'Home',
    // component: (resolve) => require(['@/views/config'], resolve),
    component: (resolve) => require(['@/views/common/tab-bar/home'], resolve),
    meta: {
      tabIndex: 0,
      index: 1,
      headerObj: {
        title: '首页',
        optionsL: {
          isShow: false
        },
        optionsR: {
          isShow: true,
          component: (resolve) => require(['@/components/ScanQRcode'], resolve),
          icon: ''
        }
      }
    }
  },

  // { path: '/campus', name: 'Campus', component: Campus, meta: { index: 1, keepAlive: true }},
  {
    path: '/campus', name: 'Campus',
    component: resolve => require(['@/views/common/tab-bar/campus'], resolve),
    meta: {
      tabIndex: 1,
      index: 1
    }
  },
  {
    path: '/common/tab-bar/news', name: 'NewsList',
    component: () => import('@/views/common/tab-bar/news'),
    meta: {
      tabIndex: 2,
      index: 1,
      headerObj: {
        title: '消息',
        optionsL: {
          isShow: false
        }
      }
    }
  },
  {
    path: '/common/tab-bar/my', name: 'My',
    component: () => import('@/views/common/tab-bar/my'),
    meta: {
      tabIndex: 3,
      index: 1,
      headerObj: {
        title: '个人中心',
        optionsL: {
          isShow: false
        }
      }
    }
  }
]

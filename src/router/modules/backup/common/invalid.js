/**
 * 无效、错误、异常 提示页面
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */

// const Invalid500 = () => import('@/views/common/invalid/500.vue')
// const NotJoinClass = () => import('@/views/common/invalid/not-join-class.vue')
// const NotData = () => import('@/views/common/invalid/not-data.vue')

export default [
  {
    path: '/common/invalid/500', name: 'Invalid500',
    component: resolve => require(['@/views/common/invalid/500.vue'], resolve),
    meta: {
      index: 5,
      headerObj: {
        title: '提示'
      }
    }
  },
  {
    path: '/common/invalid/not-join-class', name: 'NotJoinClass',
    component: resolve => require(['@/views/common/invalid/not-join-class.vue'], resolve),
    meta: {
      index: 5,
      headerObj: {
        title: '提示'
      }
    }
  },
  {
    path: '/common/invalid/not-data', name: 'NotData',
    component: resolve => require(['@/views/common/invalid/not-data.vue'], resolve),
    meta: {
      index: 5,
      headerObj: {
        title: '提示'
      }
    }
  },
  {
    path: '/common/invalid/scan-data', name: 'scanData',
    component: resolve => require(['@/views/common/template/scanResult.vue'], resolve),
    meta: {
      index: 5,
      headerObj: {
        title: '扫描结果',
        optionsL: {
          isShow: true
        }
      }
    }
  }

]

/**
 * login 登陆，注册，修改密码
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */
// const Register = () => import('@/views/common/user/register.vue')
// const Login = () => import('@/views/common/user/login')
// const ChangePwd = () => import('@/views/common/user/change-pwd.vue')
// const ChangePhonenum = () => import('@/views/common/user/change-phonenum.vue')
// const ChangePhoneConfirm = () => import('@/views/common/user/change-phonenum-confirm.vue')

export default [
  {
    path: '/common/user/register', name: 'Register',
    component: resolve => require(['@/views/common/user/register.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '注册'
      }
    }
  },
  { path: '/common/user/login', name: 'Login',
    component: resolve => require(['@/views/common/user/login'], resolve),
    meta: { index: 2 }},
  {
    path: '/common/user/change-pwd', name: 'ChangePwd',
    component: resolve => require(['@/views/common/user/change-pwd.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '修改密码'
      }
    }
  },
  {
    path: '/common/user/change-phone-number-input', name: 'ChangePhonenum',
    component: resolve => require(['@/views/common/user/change-phonenum.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '修改手机号码'
      }
    }
  },
  {
    path: '/common/user/change-phone-number-input-confirm', name: 'ChangePhoneConfirm',
    component: resolve => require(['@/views/common/user/change-phonenum-confirm.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '绑定新手机号'
      }
    }
  },
  {
    path: '/common/user/change-unit-role/index', name: 'ChangeUnitRole',
    component: resolve => require(['@/views/common/user/change-unit-role/index.vue'], resolve),
    meta: {
      index: 3,
      headerObj: {
        title: '切换角色'
      }
    }
  }

]

/**
 * recipes 食谱
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */

// common
// const kindergartenList = () => import('@/views/common/gardent/index.vue') // 选择幼儿园
// const kindergartenSelect = () => import('@/views/common/gardent/signup')

export default [
  { path: '/common/gardent/index', name: 'kindergartenList',
    component: resolve => require(['@/views/common/gardent/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '选择幼儿园'
      }
    }
  },
  { path: '/common/gardent/signup', name: 'kindergartenSelect',
    component: resolve => require(['@/views/common/gardent/signup'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '我的报名'
      }
    }
  }
]

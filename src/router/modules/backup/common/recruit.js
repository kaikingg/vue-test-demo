
/**
 * recruit 招生
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */

// const RecruitInfo = () => import('@/views/common/recruit/index.vue') // 招生模块首页
// const CommonProblem = () => import('@/views/common/recruit/common-problem.vue') // 常见问题
// const EnrollResult = () => import('@/views/common/recruit/enroll-result.vue')
// const EnrollRecord = () => import('@/views/common/recruit/enroll-record.vue')
// const Article = () => import('@/views/common/recruit/article.vue')
// const EnrollIntention = () => import('@/views/common/recruit/enroll-intention.vue')
// const FillInInfo2 = () => import('@/views/common/recruit/fill-in-2base-info.vue')
// const FillInInfo3 = () => import('@/views/common/recruit/fill-in-3household-info.vue')
// const FillInInfo4 = () => import('@/views/common/recruit/fill-in-4medicalHistory-info.vue')
// const FillInInfo5 = () => import('@/views/common/recruit/fill-in-5keeper-info.vue')
// const ConfirmEnrollInfo = () => import('@/views/common/recruit/confirm-enroll-info.vue') // 新增报名 确认 详情页面
// const selectNationalType = () => import('@/views/common/recruit/select-national-type.vue') // 滚动定位-模板

export default [
  // 招生
  {
    path: '/common/recruit/index', name: 'RecruitInfo',
    component: resolve => require(['@/views/common/recruit/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '招生信息'
      }
    }
  },
  {
    path: '/common/recruit/common-problem', name: 'CommonProblem',
    component: resolve => require(['@/views/common/recruit/common-problem.vue'], resolve),
    meta: {
      index: 3,
      headerObj: {
        title: '常见问题'
      }
    }
  },
  { path: '/common/recruit/enroll-result', name: 'EnrollResult',
    component: resolve => require(['@/views/common/recruit/enroll-result.vue'], resolve),
    meta: { index: 2 }},
  {
    path: '/common/recruit/enroll-record', name: 'EnrollRecord',
    component: resolve => require(['@/views/common/recruit/enroll-record.vue'], resolve),
    meta: {
      index: 3,
      headerObj: {
        title: '报名记录'
      }
    }
  },
  { path: '/common/recruit/article', name: 'Article',
    component: resolve => require(['@/views/common/recruit/article.vue'], resolve),
    meta: { index: 20 }},
  {
    path: '/common/recruit/enroll-intention', name: 'EnrollIntention',
    component: resolve => require(['@/views/common/recruit/enroll-intention.vue'], resolve),
    meta: {
      index: 20,
      headerObj: {
        title: '就读意向',
        optionsR: {
          isShow: true,
          text: '(1/5)',
          icon: ''
        }
      }
    }
  },
  { path: '/common/recruit/fill-in-base-info', name: 'FillInInfo2',
    component: resolve => require(['@/views/common/recruit/fill-in-2base-info.vue'], resolve),
    meta: { index: 4 }},
  { path: '/common/recruit/fill-in-household-info', name: 'FillInInfo3',
    component: resolve => require(['@/views/common/recruit/fill-in-3household-info.vue'], resolve),
    meta: { index: 6 }},
  {
    path: '/common/recruit/select-national-type', name: 'selectNationalType',
    component: resolve => require(['@/views/common/recruit/select-national-type.vue'], resolve),
    meta: {
      index: 6.1,
      keepAlive: false,
      headerObj: {
        title: '选择民族'
      }
    }
  },
  { path: '/common/recruit/fill-in-medicalHistory-info', name: 'FillInInfo4',
    component: resolve => require(['@/views/common/recruit/fill-in-4medicalHistory-info.vue'], resolve),
    meta: { index: 8 }},
  { path: '/common/recruit/fill-in-keeper-info', name: 'FillInInfo5',
    component: resolve => require(['@/views/common/recruit/fill-in-5keeper-info.vue'], resolve),
    meta: { index: 10 }},
  {
    path: '/common/recruit/confirm-enroll-info', name: 'ConfirmEnrollInfo',
    component: resolve => require(['@/views/common/recruit/confirm-enroll-info.vue'], resolve),
    meta: {
      index: 18,
      headerObj: {
        title: 'confirm-enroll-info'
      }
    }
  }
]

/**
 * 正常业务
 * meta:{
 *  index : 路由层级数，页面切换特效
 * }
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */

// const Home = () => import('@/views/common/tab-bar/home') // 首页
// const Campus = () => import('@/views/common/tab-bar/campus') // 校园
// const MailLink = () => import('@/views/common/tab-bar/mail-link')
// const My = () => import('@/views/common/tab-bar/my') // 我的
// const NewsList = () => import('@/views/common/tab-bar/news') // 消息
// // const DynamicType = () => import('@/views/common/tab-bar/dynamic') // 似乎不再使用

// const Duty = () => import('@/views/common/duty/index.vue')
// const Rules = () => import('@/views/common/rules/index.vue')
// const MailNews = () => import('@/views/common/mail/mail-news.vue')
// const ChatWindow = () => import('@/views/common/mail/chat-window.vue')

// const LinkNameList = () => import('@/views/staff/link-name-list/index')
// const classType = () => import('@/views/staff/class-type/index.vue')

// // test
// const Test1 = () => import('@/views/common/test/test1') // test
// const Test2 = () => import('@/views/common/test/test2') // test

export default [
  {
    path: '/common/tab-bar/mail-link', name: 'MailLink',
    component: () => import('@/views/common/tab-bar/mail-link'),
    meta: { index: 2, keepAlive: false }
  },
  {
    path: '/pay_demo',
    name: 'PayDemo',
    component: () => import('@/views/pay-demo'),
    meta: {
      index: 2,
      headerObj: {
        title: 'PayDemo'
      }
    }
  },
  // {
  //   path: '/config',
  //   name: 'ScrollDemo',
  //   component: () => import('@/views/config'),
  //   meta: {
  //     index: 2,
  //     headerObj: {
  //       title: 'ScrollDemo'
  //     }
  //   }
  // },
  {
    path: '/redirect/:path*',
    name: 'Redirect',
    component: () => import('@/views/common/redirect/index')
  },
  {
    path: '/common/duty/index', name: 'Duty',
    component: () => import('@/views/common/duty/index.vue'),
    meta: {
      index: 2,
      headerObj: {
        title: '岗位职责'
      }
    }
  },
  {
    path: 'common/rules/index', name: 'Rules',
    component: () => import('@/views/common/rules/index.vue'),
    meta: {
      index: 2,
      headerObj: {
        title: '规章制度'
      }
    }
  },
  {
    path: '/staff/link-name-list/index', name: 'LinkNameList',
    component: () => import('@/views/staff/link-name-list/index'),
    meta: { index: 2 }},
  {
    path: '/staff/class-type', name: 'classType',
    component: () => import('@/views/staff/class-type/index.vue'),
    meta: {
      index: 2
    }
  },
  {
    path: '/common/mail/chat-window', name: 'ChatWindow',
    component: () => import('@/views/common/mail/chat-window.vue'),
    meta: { index: 6 }},
  {
    path: '/mail-news', name: 'MailNews',
    component: () => import('@/views/common/mail/mail-news.vue'),
    meta: {
      index: 2,
      headerObj: {
        title: '消息记录'
      }
    }
  }

]

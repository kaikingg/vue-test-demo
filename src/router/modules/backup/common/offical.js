/**
 * offical 微官网
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */

// const OfficalNet = () => import('@/views/common/offical/index.vue')
// const OfficalIntroduce = () => import('@/views/common/offical/introduce.vue')
// const OfficalNews = () => import('@/views/common/offical/news.vue')
// const OfficalSpecial = () => import('@/views/common/offical/special.vue')
// const OfficalNewsDetail = () => import('@/views/common/offical/news-detail.vue')

export default [
  {
    path: '/common/offical/index', name: 'OfficalNet',
    component: resolve => require(['@/views/common/offical/index.vue'], resolve),
    meta: { index: 2 },
    children: [
      { path: 'introduce', name: 'OfficalIntroduce',
        component: resolve => require(['@/views/common/offical/introduce.vue'], resolve),
        meta: { index: 3 }},
      { path: 'news', name: 'OfficalNews',
        component: resolve => require(['@/views/common/offical/news.vue'], resolve),
        meta: { index: 3 }},
      { path: 'special', name: 'OfficalSpecial',
        component: resolve => require(['@/views/common/offical/special.vue'], resolve),
        meta: { index: 3 }}
    ] },
  {
    path: '/common/offica/news-detail', name: 'OfficalNewsDetail',
    component: resolve => require(['@/views/common/offical/news-detail.vue'], resolve),
    meta: { index: 4 }}

]

// const GardentNotice = () => import('@/views/common/news/gardent-news')
// const PlatFormNotice = () => import('@/views/common/news/platform-news')
// const ChatNotice = () => import('@/views/common/news/chat-news')
// const PlateformNoticeDetail = () => import('@/views/common/news/plateform-news-detail')

export default [
  {
    path: '/common/news/gardent-news', name: 'GardentNotice',
    component: resolve => require(['@/views/common/news/gardent-news'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '园所消息'
      }
    }
  },
  {
    path: '/common/news/platform-news', name: 'PlatFormNotice',
    component: resolve => require(['@/views/common/news/platform-news'], resolve),
    meta: {
      index: 3,
      headerObj: {
        title: '平台消息'
      }
    }
  },
  {
    path: '/common/news/platform-news-detail', name: 'PlateformNoticeDetail',
    component: resolve => require(['@/views/common/news/plateform-news-detail'], resolve),
    meta: {
      index: 4,
      headerObj: {
        title: '平台消息详情'
      }
    }
  },
  {
    path: '/common/news/chat-news', name: 'ChatNotice',
    component: resolve => require(['@/views/common/news/chat-news'], resolve),
    meta: {
      index: 2
    }
  }
]

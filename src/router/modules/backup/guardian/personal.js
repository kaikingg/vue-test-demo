/**
 * 正常业务
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */

// const Personal = () => import('@/views/guardian/personal/index')
// const PersonalSetting = () => import('@/views/guardian/personal/setting')
// const Feedback = () => import('@/views/guardian/personal/feedback')

export default [
  {
    path: '/guardian/personal/index', name: 'Personal',
    component: resolve => require(['@/views/guardian/personal/index'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '个人资料'
      }
    }
  },
  {
    path: '/guardian/personal/setting', name: 'PersonalSetting',
    component: resolve => require(['@/views/guardian/personal/setting'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '设置'
      }
    }
  },
  {
    path: '/guardian/personal/feedback', name: 'Feedback',
    component: resolve => require(['@/views/guardian/personal/feedback'], resolve),
    meta: {
      index: 3,
      headerObj: {
        title: '意见反馈'
      }
    }
  }
]

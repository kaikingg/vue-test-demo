/**
 * recipes 食谱
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */

// guardian
// const RecipesList = () => import('@/views/guardian/recipes/index.vue') // 忌口登记
// const RecipesManager = () => import('@/views/guardian/recipes/recipes-manager.vue') // 忌口配置

export default [
  {
    path: '/guardian/recipes/index',
    name: 'RecipesList',
    component: resolve => require(['@/views/guardian/recipes/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '忌口登记'
      }
    }
  },
  {
    path: '/guardian/recipes/recipes-manager',
    name: 'RecipesManager',
    component: resolve => require(['@/views/guardian/recipes/recipes-manager.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '忌口配置'
      }
    }
  }
]

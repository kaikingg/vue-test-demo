/**
 * recipes 食谱
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */

// common
// const kindergartenList = () => import('@/views/common/gardent/index.vue') // 选择幼儿园
// const kindergartenSelect = () => import('@/views/common/gardent/signup')

export default [
  // { path: '/guardian/activity/select-active', name: 'guardianSelectActive',
  { path: '/guardian/activity/signup/index', name: 'guardianSelectActive',
    component: resolve => require(['@/views/guardian/activity/signup/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '选择活动'
      }
    }
  },
  // { path: '/guardian/activity/my-select-active', name: 'guardianMySelectedActive',
  { path: '/guardian/activity/index', name: 'guardianMySelectedActive',
    component: resolve => require(['@/views/guardian/activity/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '我报名的活动'
      }
    }
  },
  { path: '/guardian/activity/detail', name: 'guardianActivityDetail',
    component: resolve => require(['@/views/guardian/activity/detail.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '活动详情'
      }
    }
  },
  // { path: '/guardian/activity/select-active-detail', name: 'guardianSelectedActivityDetail',
  { path: '/guardian/activity/signup/detail', name: 'guardianSelectedActivityDetail',
    component: resolve => require(['@/views/guardian/activity/signup/detail.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '活动详情'
      }
    }
  },
  // { path: '/guardian/activity/sign-up-file', name: 'guardianSignUpFileAdd',
  { path: '/guardian/activity/signup/add', name: 'guardianSignUpFileAdd',
    component: resolve => require(['@/views/guardian/activity/signup/add.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '填写报名档案'
      }
    }
  },
  { path: '/guardian/activity/signup/select-course-time', name: 'guardianCourseSelect',
    component: resolve => require(['@/views/guardian/activity/signup/selectTime.vue'], resolve),
    meta: {
      index: 3,
      headerObj: {
        title: '选择时间段'
      }
    }
  },
  { path: '/guardian/activity/signup/pay-up', name: 'guardianPayUp',
    component: resolve => require(['@/views/guardian/activity/signup/pay-up.vue'], resolve),
    meta: {
      index: 4,
      headerObj: {
        title: '支付页面'
      }
    }
  },
  { path: '/guardian/activity/signup/payment-code', name: 'guardianPaymentCode',
    component: resolve => require(['@/views/guardian/activity/signup/payment-code.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '支付码'
      }
    }
  },
  // { path: '/guardian/activity/verify-status', name: 'guardianVerifyStatus',
  { path: '/guardian/activity/signup/sign-up-status', name: 'guardianVerifyStatus',
    component: resolve => require(['@/views/guardian/activity/signup/sign-up-status.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '报名状态'
      }
    }
  },
  { path: '/guardian/activity/sign-up-state', name: 'mySignUpList',
    component: resolve => require(['@/views/guardian/activity/sign-up-state.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '报名情况'
      }
    }
  },
  { path: '/guardian/activity/share-list', name: 'myShareList',
    component: resolve => require(['@/views/guardian/activity/share-list.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '活动分享'
      }
    }
  },
  { path: '/guardian/activity/share-detail', name: 'guardianShareDetail',
    component: resolve => require(['@/views/guardian/activity/share/detail.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '活动分享内容'
      }
    }
  },
  { path: '/guardian/activity/feedback-publish', name: 'FeedbackPublish',
    component: resolve => require(['@/views/guardian/activity/feedback/add.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '活动反馈'
      }
    }
  },
  // { path: '/guardian/activity/feedback-list', name: 'guardianFeedbackList',
  { path: '/guardian/activity/feedback/index', name: 'guardianFeedbackList',
    component: resolve => require(['@/views/guardian/activity/feedback/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '反馈列表'
      }
    }
  },
  { path: '/guardian/activity/feedback-detail', name: 'myFeedbackDetail',
    component: resolve => require(['@/views/guardian/activity/feedback/detail.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '反馈详情'
      }
    }
  },
  { path: '/guardian/activity/verify', name: 'guardianVerify',
    component: resolve => require(['@/views/guardian/activity/verify.vue'], resolve),
    meta: {
      index: 5,
      headerObj: {
        title: '核对报名信息'
      }
    }
  },
  { path: '/guardian/activity/signup/statement', name: 'guardianStatement',
    component: resolve => require(['@/views/guardian/activity/signup/statement.vue'], resolve),
    meta: {
      index: 6,
      headerObj: {
        title: '服务条款'
      }
    }
  }
]

/**
 * album 相册
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */
// const DirectorMail = () => import('@/views/guardian/director-mail/index.vue')
// const DirectorMailAdd = () => import('@/views/guardian/director-mail/add.vue')
// const DirectorMailDetail = () => import('@/views/guardian/director-mail/detail.vue')

export default [
  {
    path: '/guardian/director-mail/index', name: 'DirectorMail',
    component: resolve => require(['@/views/guardian/director-mail/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '园长信箱'
      }
    }
  },
  {
    path: '/guardian/director-mail/add', name: 'DirectorMailAdd',
    component: resolve => require(['@/views/guardian/director-mail/add.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '写信'
      }
    }
  },
  {
    path: '/guardian/director-mail/detail', name: 'DirectorMailDetail',
    component: resolve => require(['@/views/guardian/director-mail/detail.vue'], resolve),
    meta: {
      index: 3,
      headerObj: {
        title: '信息详情'
      }
    }
  }
]

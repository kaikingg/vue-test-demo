/**
 * notice 通知
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */
// guardian
// const NoticeAllParent = () => import('@/views/guardian/notice/index.vue')
// const NoticeAllParentDetail = () => import('@/views/guardian/notice/notice-all-parent-detail.vue')

export default [
  { path: '/guardian/notice/index', name: 'NoticeAllParent',
    component: resolve => require(['@/views/guardian/notice/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '通知'
      }
    }
  },
  { path: '/guardian/notice-all-parent-detail', name: 'NoticeAllParentDetail',
    component: resolve => require(['@/views/guardian/notice/notice-all-parent-detail.vue'], resolve),
    meta: {
      index: 4,
      headerObj: {
        title: '通知详情'
      }
    }
  }

]

/**
 * homework 作业
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */

// guardian
// const HomeworkManageGuardian = () => import('@/views/guardian/homework/index')
// const HomeworkDo = () => import('@/views/guardian/homework/homework-do.vue')
// const HomeworkDone = () => import('@/views/guardian/homework/homework-done.vue')
// const TempHomeworkDetail = () => import('@/views/guardian/homework/temp-homework-detail')

export default [
  {
    path: '/guardian/homework/index', name: 'HomeworkManageGuardian',
    component: resolve => require(['@/views/guardian/homework/index'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '亲子任务管理'
      }
    }
  },
  {
    path: '/guardian/homework-do', name: 'HomeworkDo',
    component: resolve => require(['@/views/guardian/homework/homework-do.vue'], resolve),
    meta: {
      index: 5,
      headerObj: {
        title: '做亲子任务'
      }
    }
  },
  {
    path: '/guardian/homework-done', name: 'HomeworkDone',
    component: resolve => require(['@/views/guardian/homework/homework-done.vue'], resolve),
    meta: {
      index: 5,
      headerObj: {
        title: '已完成亲子任务'
      }
    }
  },
  {
    path: '/guardian/temp-homework-detail', name: 'TempHomeworkDetail',
    component: resolve => require(['@/views/guardian/homework/temp-homework-detail'], resolve),
    meta: {
      index: 5,
      headerObj: {
        title: '已完成亲子任务'
      }
    }
  }
]

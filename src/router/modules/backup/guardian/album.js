/**
 * album 相册
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */
// 家长
// const AlbumDetail = () => import('@/views/guardian/album/detail.vue')
// const AlbumList = () => import('@/views/guardian/album/index.vue')

export default [
  {
    path: '/guardian/album/detail', name: 'GuardianAlbumDetail',
    component: resolve => require(['@/views/guardian/album/detail.vue'], resolve),
    meta: {
      index: 3,
      headerObj: {
        title: '相册详情'
      }
    }
  },
  {
    path: '/guardian/album/index', name: 'GuardianAlbumList',
    component: resolve => require(['@/views/guardian/album/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '相册列表'
      }
    }
  }
]

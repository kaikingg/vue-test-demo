/**
 * 家长
 * attendance 请假+考勤
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */
// const askforleave = () => import('@/views/guardian/attendance/index.vue') // 请假
// const AttendanceRecord = () => import('@/views/guardian/attendance/attendance-record.vue') // 个人考勤记录
// const askforleaveRecord = () => import('@/views/guardian/attendance/askForLeave-record.vue') // 请假记录列表
// const askforleaveDetail = () => import('@/views/guardian/attendance/askForLeave-detail.vue') // 请假详情
const attendance = [
  {
    path: '/guardian/attendance/index', name: 'askforleave',
    component: resolve => require(['@/views/guardian/attendance/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '请假'
      }
    }
  },
  {
    path: '/guardian/attendance/attendance-record', name: 'AttendanceRecord',
    component: resolve => require(['@/views/guardian/attendance/attendance-record.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '考勤记录'
      }
    }
  },
  {
    path: '/guardian/attendance/askforleave-record', name: 'askforleaveRecord',
    component: resolve => require(['@/views/guardian/attendance/askForLeave-record.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '请假记录'
      }
    }
  },
  { path: '/guardian/attendance/askforleave-detail', name: 'askforleaveDetail',
    component: resolve => require(['@/views/guardian/attendance/askForLeave-detail.vue'], resolve),
    meta: { index: 2 }}
]

export default attendance

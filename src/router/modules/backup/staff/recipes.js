/**
 * recipes 食谱
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */

// leader & staff
// const RecipesNotice = () => import('@/views/staff/recipes/index.vue') // 食谱预告

// leader
// const RecipesManagerCheck = () => import('@/views/staff/recipes/recipes-manage-check')

// staff
// const RecipesStaff = () => import('@/views/staff/recipes/recipes-staff') //  忌口管理
// const RecipesStaffList = () => import('@/views/staff/recipes/recipes-staff-list.vue') // 忌口列表
// const RecipesTeacher = () => import('@/views/staff/recipes/recipes-teacher.vue') // 忌口统计
// const SelectFood = () => import('@/views/staff/recipes/select-food.vue') // 食谱预告 - 选择食物
// const PublishRecipesNotice = () => import('@/views/staff/recipes/publish.vue') // 食谱预告 - 发布食谱预告

export default [
  {
    path: '/staff/recipes/select-food', name: 'SelectFood',
    component: resolve => require(['@/views/staff/recipes/select-food.vue'], resolve),
    meta: {
      index: 2
    }
  },
  {
    path: '/staff/recipes/recipes-teacher',
    name: 'RecipesTeacher',
    component: resolve => require(['@/views/staff/recipes/recipes-teacher.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '忌口统计'
      }
    }
  },
  {
    path: '/staff/recipes/recipes-staff',
    name: 'RecipesStaff',
    component: resolve => require(['@/views/staff/recipes/recipes-staff'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '忌口管理'
      }
    }
  },
  { path: '/staff/recipes/recipes-staff-list', name: 'RecipesStaffList',
    component: resolve => require(['@/views/staff/recipes/recipes-staff-list.vue'], resolve),
    meta: {
      index: 3,
      headerObj: {
        title: '忌口列表'
      }
    }
  },
  { path: '/staff/recipes/recipes-manager-check', name: 'RecipesManagerCheck',
    component: resolve => require(['@/views/staff/recipes/recipes-manage-check'], resolve),
    meta: {
      index: 3
      // headerObj: {
      //   title: '查看忌口统计'
      // }
    }
  },
  {
    path: '/staff/recipes/index', name: 'RecipesNotice',
    component: resolve => require(['@/views/staff/recipes/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '食谱预告'
      }
    }
  },
  {
    path: '/staff/recipes/publish', name: 'PublishRecipesNotice',
    component: resolve => require(['@/views/staff/recipes/publish.vue'], resolve),
    meta: {
      index: 3,
      headerObj: {
        title: '发布食谱'
      }
    }
  }
]

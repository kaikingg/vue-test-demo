
/**
 * recruit 招生
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */
// const RecruitSummary = () => import('@/views/staff/recruit/index.vue')
// const ClassDetail = () => import('@/views/staff/recruit/class-detail.vue')
// const SplitClass = () => import('@/views/staff/recruit/split-class.vue') // 分班
// const StudentDetail = () => import('@/views/staff/recruit/student-detail.vue') // 学生详情

export default [
  // 招生
  {
    path: '/staff/recruit/index', name: 'RecruitSummary',
    component: resolve => require(['@/views/staff/recruit/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '招生汇总'
      }
    }
  },
  {
    path: '/staff/recruit/class-detail', name: 'ClassDetail',
    component: resolve => require(['@/views/staff/recruit/class-detail.vue'], resolve),
    meta: {
      index: 3,
      headerObj: {
        title: '高级搜索'
      }
    }
  },
  {
    path: '/staff/recruit/split-class', name: 'SplitClass',
    component: resolve => require(['@/views/staff/recruit/split-class.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '分班'
      }
    }
  },
  {
    path: '/staff/recruit/student-detail', name: 'StudentDetail',
    component: resolve => require(['@/views/staff/recruit/student-detail.vue'], resolve),
    meta: {
      index: 4,
      headerObj: {
        title: '幼儿详情信息'
      }
    }
  }
]

/**
 * recipes 食谱
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */

// common
// const kindergartenList = () => import('@/views/common/gardent/index.vue') // 选择幼儿园
// const kindergartenSelect = () => import('@/views/common/gardent/signup')

export default [
  { path: '/staff/activity/index', name: 'activityManager',
    component: resolve => require(['@/views/staff/activity/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '活动管理'
      }
    }
  },
  // { path: '/staff/activity/my-charge-active', name: 'myChargeActivity',
  { path: '/staff/activity/my-active', name: 'myChargeActivity',
    component: resolve => require(['@/views/staff/activity/my-active.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '我负责的活动'
      }
    }
  },
  { path: '/staff/activity/add', name: 'activePublish',
    component: resolve => require(['@/views/staff/activity/add.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '发布活动'
      }
    }
  },
  { path: '/staff/activity/detail', name: 'activityDetail',
    component: resolve => require(['@/views/staff/activity/detail.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '活动详情'
      }
    }
  },
  { path: '/staff/activity/sign-up-state', name: 'signUpState',
    component: resolve => require(['@/views/staff/activity/sign-up-state.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '报名情况'
      }
    }
  },
  { path: '/staff/activity/share-list', name: 'ShareList',
    component: resolve => require(['@/views/staff/activity/share-list.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '活动分享'
      }
    }
  },
  // { path: '/staff/activity/share-publish', name: 'SharePublish',
  { path: '/staff/activity/share/add', name: 'SharePublish',
    component: resolve => require(['@/views/staff/activity/share/add.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '发布活动分享'
      }
    }
  },
  { path: '/staff/activity/share-detail', name: 'ShareDetail',
    component: resolve => require(['@/views/staff/activity/share/detail.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '活动分享内容'
      }
    }
  },
  { path: '/staff/activity/active-publish-object', name: 'ActivePublishObject',
    component: resolve => require(['@/views/staff/activity/active-publish-object.vue'], resolve),
    meta: {
      index: 5,
      headerObj: {
        title: '选择发送对象'
      }
    }
  },
  { path: '/staff/activity/feedback-list', name: 'FeedbackList',
    component: resolve => require(['@/views/staff/activity/feedback/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '反馈列表'
      }
    }
  },
  { path: '/staff/activity/feedback-detail', name: 'FeedbackDetail',
    component: resolve => require(['@/views/staff/activity/feedback/detail.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '反馈详情'
      }
    }
  },
  { path: '/staff/activity/verify', name: 'Verify',
    component: resolve => require(['@/views/staff/activity/verify.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '报名审核'
      }
    }
  }
]

/**
 * homework 作业
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */
// const HomeworkManage = () => import('@/views/staff/homework/index.vue')
// const HomeworkStudentList = () => import('@/views/staff/homework/homework-student-list.vue')
// const HomeworkDetail = () => import('@/views/staff/homework/homework-detail.vue') // 似乎没用到 HomeworkDetail
// const HomeworScore = () => import('@/views/staff/homework/homework-score.vue')
// const HomeworkSituation = () => import('@/views/staff/homework/homework-situation.vue')

// const HomeworkPublish = () => import('@/views/staff/homework/homework-publish.vue')

// leader
// const HomeworkManageLeader = () => import('@/views/staff/homework/leader/index')

export default [
  // {
  //   path: '/staff/homework/list', name: 'HomeworkManageList',
  //   component: resolve => require(['@/views/staff/homework/list.vue'], resolve),
  //   meta: {
  //     index: 2,
  //     headerObj: {
  //       title: '亲子任务-list'
  //     }
  //   }
  // },
  {
    path: '/staff/homework/index', name: 'HomeworkManage',
    component: resolve => require(['@/views/staff/homework/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '亲子任务管理'
      }
    }
  },
  {
    path: '/staff/homework/homework-student-list', name: 'HomeworkStudentList',
    component: resolve => require(['@/views/staff/homework/homework-student-list.vue'], resolve),
    meta: {
      index: 4,
      headerObj: {
        title: '学生亲子任务列表'
      }
    }
  },
  {
    path: '/staff/homework/homework-detail', name: 'HomeworkDetail',
    component: resolve => require(['@/views/staff/homework/homework-detail.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '亲子任务详情'
      }
    }
  },
  {
    path: '/staff/homework/homework-score', name: 'HomeworScore',
    component: resolve => require(['@/views/staff/homework/homework-score.vue'], resolve),
    meta: {
      index: 6,
      headerObj: {
        title: '亲子任务评分'
      }
    }
  },
  {
    path: '/staff/homework/homework-situation', name: 'HomeworkSituation',
    component: resolve => require(['@/views/staff/homework/homework-situation.vue'], resolve),
    meta: {
      index: 3,
      headerObj: {
        title: '亲子任务详情'
      }
    }
  },
  {
    path: '/staff/homework/homework-publise', name: 'HomeworkPublish',
    component: resolve => require(['@/views/staff/homework/homework-publish.vue'], resolve),
    meta: {
      index: 3,
      headerObj: {
        title: '发布亲子任务'
      }
    }
  },
  {
    path: '/staff/homework/leader/index', name: 'HomeworkManageLeader',
    component: resolve => require(['@/views/staff/homework/leader/index'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '亲子任务管理'
      }
    }
  }
]

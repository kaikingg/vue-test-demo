/**
 * album 相册
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */
// const LeaderDirectorMail = () => import('@/views/staff/director-mail/index.vue')
// const LeaderDirectorMailDetail = () => import('@/views/staff/director-mail/detail.vue')

export default [
  {
    path: '/staff/director-mail/index', name: 'LeaderDirectorMail',
    component: resolve => require(['@/views/staff/director-mail/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '园长信箱'
      }
    }
  },
  {
    path: '/staff/director-mail/detail', name: 'LeaderDirectorMailDetail',
    component: resolve => require(['@/views/staff/director-mail/detail.vue'], resolve),
    meta: {
      index: 3,
      headerObj: {
        title: '信息详情'
      }
    }
  }
]

/**
 * archives 学籍档案
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */
// const SchoolStatusManageList = () => import('@/views/staff/archives/index.vue')
// const SchoolStatusManage = () => import('@/views/staff/archives/school-status-manage.vue')
// const SchoolStatusDetail = () => import('@/views/staff/archives/school-status-detail.vue')
// const TeacherManageLlist = () => import('@/views/staff/archives/teacher-manage-list.vue')
// 管理者
// const StudentArchivesManage = () => import('@/views/staff/archives/students-archives-manage/index.vue')

const attendance = [
  {
    path: '/staff/archives/index', name: 'SchoolStatusManageList',
    component: resolve => require(['@/views/staff/archives/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '学籍管理'
      }
    }
  },
  {
    path: '/staff/archives/school-status-manage', name: 'SchoolStatusManage',
    component: resolve => require(['@/views/staff/archives/school-status-manage.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '学籍信息'
      }
    }
  },
  {
    path: '/staff/archives/school-status-detail', name: 'SchoolStatusDetail',
    component: resolve => require(['@/views/staff/archives/school-status-detail.vue'], resolve),
    meta: {
      index: 3,
      headerObj: {
        title: '学籍详情'
      }
    }
  },
  {
    path: '/staff/archives/teacher-manage-list', name: 'TeacherManageLlist',
    component: resolve => require(['@/views/staff/archives/teacher-manage-list.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '教师档案'
      }
    }
  },
  {
    path: '/staff/archives/students-archives-manage/index', name: 'StudentArchivesManage',
    component: resolve => require(['@/views/staff/archives/students-archives-manage/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '学籍管理'
      }
    }
  }
]

export default attendance

/**
 * attendance 请假+考勤
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */

// staff
// const askforleaveListByTeacher = () => import('@/views/staff/attendance/index.vue') // 员工查看请假列表
// const askforleaveDetailByTeacher = () => import('@/views/staff/attendance/askForLeave-detail-byTeacher.vue') // 请假记录

// leader
// const AllClassAtendanceCount = () => import('@/views/staff/attendance/leader/index.vue')// 全园考勤

// const AttendanceCallName = () => import('@/views/staff/attendance/call-name.vue') // 点名
// const ClassAttendanceRecord = () => import('@/views/staff/attendance/class-attendance-record.vue') // 班级考勤记录
// const askforleaveListByManage = () => import('@/views/staff/attendance/askForLeave-list-byManage.vue') // 请假记录列表
// const SelectGrade = () => import('@/views/staff/attendance/select-grade.vue')// 选择班级类别
// const SelectClass = () => import('@/views/staff/attendance/select-class.vue') // 选择班级类别

const attendance = [
  // 请假+考勤
  {
    path: '/staff/attendance/index', name: 'askforleaveListByTeacher',
    component: resolve => require(['@/views/staff/attendance/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '请假记录'
      }
    }
  },
  {
    path: '/staff/attendance/askforleave-detail-byTeacher', name: 'askforleaveDetailByTeacher',
    component: resolve => require(['@/views/staff/attendance/askForLeave-detail-byTeacher.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '请假详情'
      }
    }
  },
  {
    path: '/staff/attendance/leader/index', name: 'AllClassAtendanceCount',
    component: resolve => require(['@/views/staff/attendance/leader/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '全园考勤'
      }
    }
  },
  {
    path: '/staff/attendance/call-name', name: 'AttendanceCallName',
    component: resolve => require(['@/views/staff/attendance/call-name.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '点名'
      }
    }
  },
  {
    path: '/staff/attendance/class-attendance-record', name: 'ClassAttendanceRecord',
    component: resolve => require(['@/views/staff/attendance/class-attendance-record.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '班级考勤'
      }
    }
  },
  {
    path: '/staff/attendance/askforleave-list-byManage', name: 'askforleaveListByManage',
    component: resolve => require(['@/views/staff/attendance/askForLeave-list-byManage.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '请假记录'
      }
    }
  },
  {
    path: '/staff/attendance/select-grade', name: 'SelectGrade',
    component: resolve => require(['@/views/staff/attendance/select-grade.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '班级类别'
      }
    }
  }, // SelectNationType

  {
    path: '/staff/attendance/select-class', name: 'SelectClass',
    component: resolve => require(['@/views/staff/attendance/select-class.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '班级'
      }
    }
  }
]

export default attendance

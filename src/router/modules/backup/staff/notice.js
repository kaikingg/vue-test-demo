/**
 * notice 通知
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */
// teacher
// const index = () => import('@/views/staff/notice/index.vue')
// const NoticeAll = () => import('@/views/staff/notice/notice-all.vue')
// const NoticeClass = () => import('@/views/staff/notice/notice-class.vue')

// const NoticeDetail = () => import('@/views/staff/notice/notice-detail.vue')
// const NoticeMyPublish = () => import('@/views/staff/notice/notice-myPublish.vue')
// const NoticePublish = () => import('@/views/staff/notice/notice-publish.vue')
// const NoticePublishObject = () => import('@/views/staff/notice/notice-publish-object/index.vue')

// leader
// const NoticeLeader = () => import('@/views/staff/notice/leader/index.vue')
// const NoticeClassLeader = () => import('@/views/staff/notice/notice-class-leader.vue')
// const NoticeAllLeader = () => import('@/views/staff/notice/notice-all.vue')
// const NoticeMyPublishLeader = () => import('@/views/staff/notice/notice-myPublish.vue')

export default [

  { path: '/staff/notice/leader/index', name: 'NoticeLeader',
    component: resolve => require(['@/views/staff/notice/leader/index.vue'], resolve),
    meta: {
      index: 2
    },
    children: [
      { path: '/staff/notice/notice-all', name: 'NoticeAllLeader',
        component: resolve => require(['@/views/staff/notice/notice-all.vue'], resolve),
        meta: { index: 2 }},
      { path: '/staff/notice/notice-myPublish', name: 'NoticeMyPublishLeader',
        component: resolve => require(['@/views/staff/notice/notice-myPublish.vue'], resolve),
        meta: { index: 2 }}
    ]
  },
  { path: '/staff/notice/teacher', name: 'Notice',
    component: resolve => require(['@/views/staff/notice/index.vue'], resolve),
    meta: {
      index: 2
    },
    children: [
      { path: 'notice-all', name: 'NoticeAll',
        component: resolve => require(['@/views/staff/notice/notice-all.vue'], resolve),
        meta: { index: 3 }},
      { path: 'notice-myPublish', name: 'NoticeMyPublish',
        component: resolve => require(['@/views/staff/notice/notice-myPublish.vue'], resolve),
        meta: { index: 3 }},
      { path: 'notice-class', name: 'NoticeClass',
        component: resolve => require(['@/views/staff/notice/notice-class.vue'], resolve),
        meta: { index: 3 }}
    ]
  },
  { path: '/staff/notice/notice-detail', name: 'NoticeDetail',
    component: resolve => require(['@/views/staff/notice/notice-detail.vue'], resolve),
    meta: {
      index: 4,
      headerObj: {
        title: '通知详情'
      }
    }
  },
  { path: '/staff/notice/notice-class-leader', name: 'NoticeClassLeader',
    component: resolve => require(['@/views/staff/notice/notice-class-leader.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '班级通知'
      }
    }
  },
  { path: '/staff/notice/notice-publish', name: 'NoticePublish',
    component: resolve => require(['@/views/staff/notice/notice-publish.vue'], resolve),
    meta: {
      index: 4,
      headerObj: {
        title: '发布通知'
      }
    }
  },
  { path: '/staff/notice/notice-publish-object', name: 'NoticePublishObject',
    component: resolve => require(['@/views/staff/notice/notice-publish-object.vue'], resolve),
    meta: {
      index: 5,
      headerObj: {
        title: '选择发送对象'
      }
    }
  }

]

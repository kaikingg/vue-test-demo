/**
 * album 相册
 * 主要需考虑meta属性，
 * index: 页面层级，从1开始算，tab页属于1级，0级可作为日后扩展;层级关系的作用在于页面切换特效
 * headerObj: 是当前页面头部信息，详细请查看 @/components/Navigation/index.vue;
 * 注意：有的页面由于功能需求关系，不设meta，但会在页面内部通过 this.$store.dispatch('navigation/UpdateHeaderObj', headerObj)设置
 */
// 内部员工
// const AlbumDetail = () => import('@/views/staff/album/detail.vue')
// const AlbumList = () => import('@/views/staff/album/index.vue')
// const AlbumPublish = () => import('@/views/staff/album/publish.vue')
// 管理者
// const SchoolAlbumList = () => import('@/views/staff/album/school-album-list/index.vue')
// const SchoolAlbumDetail = () => import('@/views/staff/album/school-album-detail/index.vue')

export default [
  {
    path: '/staff/album/detail', name: 'AlbumDetail',
    component: resolve => require(['@/views/staff/album/detail.vue'], resolve),
    meta: {
      index: 3,
      headerObj: {
        title: '相册详情'
      }
    }
  },
  {
    path: '/staff/album/index', name: 'AlbumList',
    component: resolve => require(['@/views/staff/album/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '相册列表'
      }
    }
  },
  {
    // path: '/staff/album/school-album-list', name: 'SchoolAlbumList',
    path: '/staff/album/school-album-list/index', name: 'SchoolAlbumList',
    component: resolve => require(['@/views/staff/album/school-album-list/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '相册列表'
      }
    }
  },
  {
    path: '/staff/album/publish', name: 'AlbumPublish',
    component: resolve => require(['@/views/staff/album/publish.vue'], resolve),
    meta: {
      index: 3,
      headerObj: {
        title: '发布相册'
      }
    }
  },
  {
    path: '/staff/album/school-album-detail', name: 'SchoolAlbumDetail',
    component: resolve => require(['@/views/staff/album/school-album-detail/index.vue'], resolve),
    meta: {
      index: 2,
      headerObj: {
        title: '相册详情'
      }
    }
  }
]

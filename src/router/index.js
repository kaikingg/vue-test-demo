// 依赖导入
import Vue from 'vue'
import VueRouter from 'vue-router'
import Router from 'vue-router'
Vue.use(Router)
// 组件引入
import Home from '../views/home.vue'
import Mine from '../views/mine.vue'

const routes = [
  {
    path: '/',
    redirect: '/home'
  }, {
    path: '/home',
    component: Home
  }, {
    path: '/mine',
    component: Mine
  }
]

var router = new VueRouter({
  routes: routes
})

export default router

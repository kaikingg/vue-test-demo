// 资源汇总文件
import $ext from '@/ext'
import sys from './open/sys'
// import com from './custom'
import $request from './open/request'
import $constant from '../constant'
import $vuxTool from './open/vux-plugin'
import $common from './open/common'
import $validate from './open/validate'
import $webSocket from './open/websocket-plugin'
import WX from './open/wechat-plugin'

const debug = $constant.isDebug
const ctx = $constant.ctx
const file_ctx = $constant.file_ctx
const domain_ctx = $constant.domain_ctx
export default {
  $ext,
  sys,
  // com,
  $request,
  $constant,
  $vuxTool,
  $common,
  $validate,
  $webSocket,
  WX,
  debug,
  ctx,
  file_ctx,
  domain_ctx
}


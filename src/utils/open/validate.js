/**
 * 各种校验规定，包括手机号码，身份证号码，空格，url等等
 */

import vuxTool from '@/utils/open/vux-plugin'
import timePlugin from '@/utils/open/time-plugin'
import constant from '@/constant'

export default {
  /**
   * 合法uri
   * @param textval
   * @returns {boolean}
   */
  isURL(textval) {
    const urlregex = /^(http:\/\/|^https:\/\/|^\/\/)((\w|=|\?|\.|\/|&|-)+)/g
    return urlregex.test(textval)
  },

  /**
   * 小写字母
   * @param str
   * @returns {boolean}
   */
  isLowerCase(str) {
    const reg = /^[a-z]+$/
    return reg.test(str)
  },

  /**
   * 大写字母
   * @param str
   * @returns {boolean}
   */
  isUpperCase(str) {
    const reg = /^[A-Z]+$/
    return reg.test(str)
  },

  /**
   * 大小写字母
   * @param str
   * @returns {boolean}
   */
  validatAlphabets(str) {
    const reg = /^[A-Za-z]+$/
    return reg.test(str)
  },
  /**
   * 验证pad还是pc
   * @returns {boolean}
   */
  isPc() {
    const userAgentInfo = navigator.userAgent
    const Agents = ['Android', 'iPhone',
      'SymbianOS', 'Windows Phone',
      'iPad', 'iPod']
    let flag = true
    for (var v = 0; v < Agents.length; v++) {
      if (userAgentInfo.indexOf(Agents[v]) > 0) {
        flag = false
        break
      }
    }
    return flag
  },
  /**
   * validate email
   * @param email
   * @returns {boolean}
   */
  isEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(email)
  },
  /**
   * 判断身份证号码是否正常
   * return {flag,msg}
   * flag: true 正确， false 不正确
   * msg: 相关提示
   *
   */
  isCardid(num) {
    num = num.toUpperCase()

    // 身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X。
    if (!(/(^\d{15}$)|(^\d{17}([0-9]|X)$)/.test(num))) {
      return {
        status: false,
        text: '输入的身份证号长度不对，或者号码不符合规定！\n15位号码应全为数字，18位号码末位可以为数字或X。'
      }
    }

    if (constant.isDebug) { // 开发环境执下，不做严格校验
      return {
        status: true,
        text: '证件符合要求'
      }
    }

    // 校验位按照ISO 7064:1983.MOD 11-2的规定生成，X可以认为是数字10。
    // 下面分别分析出生日期和校验位
    var len, re, dtmBirth, dateObj, bGoodDay
    len = num.length
    if (len === 15) {
      re = new RegExp(/^(\d{6})(\d{2})(\d{2})(\d{2})(\d{3})$/)
      var arrSplit = num.match(re)

      // 检查生日日期是否正确
      dtmBirth = new Date('19' + arrSplit[2] + '/' + arrSplit[3] + '/' + arrSplit[4])
      dateObj = timePlugin.turnDate(dtmBirth)
      // bGoodDay = (dtmBirth.getYear() === Number(arrSplit[2])) && ((dtmBirth.getMonth() + 1) === Number(arrSplit[3])) && (dtmBirth.getDate() === Number(arrSplit[4]))
      bGoodDay = ((Number(dateObj.year) === Number(arrSplit[2])) &&
      (Number(dateObj.month) === Number(arrSplit[3])) &&
      (Number(dateObj.day) === Number(arrSplit[4])))
      if (!bGoodDay) {
        return {
          status: false,
          text: '输入的身份证号里出生日期不对！'
        }
      } else {
        // 将15位身份证转成18位
        // 校验位按照ISO 7064:1983.MOD 11-2的规定生成，X可以认为是数字10。
        var arrInt = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]
        var arrCh = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2']
        var nTemp = 0
        let i
        num = num.substr(0, 6) + '19' + num.substr(6, num.length - 6)
        for (i = 0; i < 17; i++) {
          nTemp += num.substr(i, 1) * arrInt[i]
        }
        num += arrCh[nTemp % 11]
        return {
          status: true,
          text: '证件符合要求'
        }
      }
    }

    if (len === 18) {
      re = new RegExp(/^(\d{6})(\d{4})(\d{2})(\d{2})(\d{3})([0-9]|X)$/)
      arrSplit = num.match(re)

      // 检查生日日期是否正确
      dtmBirth = new Date(arrSplit[2] + '/' + arrSplit[3] + '/' + arrSplit[4])
      dateObj = timePlugin.turnDate(dtmBirth)
      bGoodDay = ((Number(dateObj.year) === Number(arrSplit[2])) &&
      (Number(dateObj.month) === Number(arrSplit[3])) &&
      (Number(dateObj.day) === Number(arrSplit[4])))
      if (!bGoodDay) {
        return {
          status: false,
          text: '输入的身份证号里出生日期不对！'
        }
      } else {
        // 检验18位身份证的校验码是否正确。
        // 校验位按照ISO 7064:1983.MOD 11-2的规定生成，X可以认为是数字10。
        const arrInt = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]
        const arrCh = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2']
        let nTemp = 0
        for (let i = 0; i < 17; i++) {
          nTemp += num.substr(i, 1) * arrInt[i]
        }
        var valnum = arrCh[nTemp % 11]
        if (valnum !== num.substr(17, 1)) {
          return {
            status: false,
            text: '18位身份证的校验码不正确！'
          }
        }

        return {
          status: true,
          text: '证件符合要求'
        }
      }
    }

    return {
      status: false,
      text: '证件格式不正确，请检查!'
    }
  },
  /**
   * 判断手机号码是否正确
   */
  isMobile(phone) {
    phone = phone || ''
    var myreg = /^[1][3,4,5,6,7,8][0-9]{9}$/
    var obj = {
      flag: true,
      msg: ''
    }
    // console.log(phone)
    if (phone.length === 0) {
      obj.msg = '请输入手机号码！'
      obj.flag = false
    } else if (phone.length !== 11) {
      obj.msg = '请输入有效的手机号码！'
      obj.flag = false
    } else if (!myreg.test(phone)) {
      obj.msg = '请输入有效的手机号码！'
      obj.flag = false
    }
    return obj
  },
  /**
   * 判断姓名是否正确
   */
  isName(name) {
    var regName = /^[\u4e00-\u9fa5]{2,4}$/
    if (!regName.test(name)) return false
    return true
  },
  /**
   * 判断是否为整数
   */
  isNum(num, type) {
    let regName = /[^\d.]/g
    if (type === 1) {
      if (!regName.test(num)) return false
    } else if (type === 2) {
      regName = /[^\d]/g
      if (!regName.test(num)) return false
    }
    return true
  },
  /**
   * 判断是否为小数
   */
  isDecimals(num, type) {
    let regName = /[^\d.]/g
    if (type === 1) {
      if (!regName.test(num)) return false
    } else if (type === 2) {
      regName = /[^\d.]/g
      if (!regName.test(num)) return false
    }
    return true
  },
  /**
   * 校验密码
   */
  checkPWD(pwd) {
    // debugger
    pwd = pwd.replace(/(^\s*)|(\s*)$|(\s{1})/g, '') // 去除首尾和中间空格
    var obj = {
      statu: false,
      pwd: pwd,
      text: '密码长度为8-16位，必须是数字和字母组合'
    }
    if (!pwd) {
      obj.text = '密码不能为空'
      return obj
    }
    // const reg1 = /[!@#$%^&*()_?<>{}]{1}/
    const reg2 = /([a-zA-Z0-9]|[!@#$%^&*()_?<>{}]){6,16}/
    const reg3 = /[a-zA-Z]+/
    const reg4 = /[0-9]+/
    if ((reg2.test(pwd) || reg3.test(pwd)) && reg4.test(pwd)) {
      obj.statu = true
      obj.text = '密码通过'
      // return obj
    } else if (!reg2.test(pwd)) {
      obj.text = '密码长度为6-16位，必须是数字或字母组合'
      vuxTool.toast(obj.text)
    }
    // else if (!reg1.test(pwd)) {
    //   obj.text = '密码需包含一个特殊字符'
    //   vuxTool.toast(obj.text)
    // } else if (!reg2.test(pwd)) {
    //   obj.text = '密码长度为8-16位'
    //   vuxTool.toast(obj.text)
    // } else if (!reg3.test(pwd)) {
    //   obj.text = '密码需含有字母'
    //   vuxTool.toast(obj.text)
    // } else if (!reg4.test(pwd)) {
    //   obj.text = '密码需含有数字'
    //   vuxTool.toast(obj.text)
    // }
    return obj
  },
  /**
   * 清除字符串前后空格
   * @param {String} str
   */
  trim(str) { // 去字符串首尾空格
    if (typeof (str) === 'string') {
      str = str.replace(/(^\s*)|(\s*)$/g, '')
    }
    return str
  },
  /**
   * 判断是否为空
   * 为空 true, 不为空 false
   */
  isNull(val) {
    val = this.trim(val)
    if (val instanceof Array) {
      if (val.length === 0) return true
    } else if (val instanceof Object) {
      if (JSON.stringify(val) === '{}') return true
    } else {
      if (val === 'null' || val == null || val === 'undefined' || val === undefined || val === '') return true
      return false
    }
    /**
     *  else if (typeof (val) === 'string' || typeof (val) === 'number') {
          return false
        }
     */
  },
  /**
   * 判断数据类型
   * @param {各种数据类型} o
   */
  judgeTypeOf(o) {
    var typeArr = ['String', 'Object', 'Number', 'Array', 'Undefined', 'Function', 'Null', 'Symbol']
    let name = ''
    for (const a of typeArr) {
      name = '[object ' + a + ']'
      if (Object.prototype.toString.call(o) === name) {
        return a
      }
    }
  }
}

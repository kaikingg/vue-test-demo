/**
 * 对日期操作方法：
 * 获取日期，转换日期格式等等
 */
export default{
  /**
   * 获取时间戳
   * @param {date} 日期
   */
  getStamp(date) {
    const d = date.replace(new RegExp(/-/gm), '/') // '/'为兼容ios
    return (new Date(d)).getTime()
  },
  /**
   * 单字符格式化
   * @param str
   * @returns {string}
   */
  dateFormatter(str) {
    if (!str) return '00'
    const s = str.toString()
    return s.length > 1 ? s : '0' + s
  },
  /** 时间转换
   *
   * @param timeStamp
   * @param format
   * @returns {{year: number, month: (*|string), day: (*|string), hours: (*|string), minutes: (*|string), seconds: (*|string), week: number, weekDay: *, timeStamp: *}}
   */
  turnDate(timeStamp, format) {
    timeStamp = timeStamp || ''
    let date = ''
    if (!timeStamp) {
      date = new Date()
      timeStamp = date.getTime()
    } else {
      date = new Date(timeStamp)
    }
    const year = date.getFullYear()
    const month = this.dateFormatter((date.getMonth() + 1))
    const day = this.dateFormatter(date.getDate())
    const hours = this.dateFormatter(date.getHours())
    const minutes = this.dateFormatter(date.getMinutes())
    const seconds = this.dateFormatter(date.getSeconds())
    const week = date.getDay() // 星期
    const weekArr = {
      1: '星期一',
      2: '星期二',
      3: '星期三',
      4: '星期四',
      5: '星期五',
      6: '星期六',
      0: '星期日'
    }

    const obj = {
      year: year,
      month: month,
      day: day,
      hours: hours,
      minutes: minutes,
      seconds: seconds,
      week: week,
      weekDay: weekArr[week],
      timeStamp: timeStamp
    }
    return obj
  },
  /**
   *
   * 日期格式转成文字描述，比如：10分钟前
   *
   * @param timeStamp
   * @param currentStamp
   * @returns {string}
   */
  formatDate(timeStamp, currentStamp) {
    if (timeStamp && timeStamp.toString().length !== 13) {
      timeStamp = +new Date(timeStamp)
    }

    const thatDateObj = this.turnDate(timeStamp)
    const curDateObj = this.turnDate(currentStamp)
    const thatDate = thatDateObj.year + '-' + thatDateObj.month + '-' + thatDateObj.day
    const curDate = curDateObj.year + '-' + curDateObj.month + '-' + curDateObj.day
    let ts = '过去'

    if (thatDate === curDate) { // 当天
      const hour = Math.abs(parseInt((timeStamp - currentStamp) / 1000 / 3600)) // 小时
      const minutes = Math.abs(parseInt((timeStamp - currentStamp) / 1000 / 60)) // 分钟
      const seconds = Math.abs(parseInt((timeStamp - currentStamp) / 1000)) // 秒
      if (seconds > 0 && seconds < 60) {
        ts = parseInt(seconds) + '秒前'
      } else if (minutes > 0 && minutes < 60) {
        ts = parseInt(minutes) + '分钟前'
      } else if (hour > 0 && hour < 24) {
        ts = parseInt(hour) + '小时前'
      }
    } else { // 一天之后的
      const day = Math.abs(parseInt((timeStamp - currentStamp) / 1000 / 3600 / 24)) // 日期
      if (day < 2) {
        ts = '昨天'
      } else if (day < 30) {
        ts = parseInt(day) + '天前'
      } else {
        const curDate = this.turnDate(timeStamp)
        ts = curDate.year + '-' + curDate.month + '-' + curDate.day
      }
    }
    return ts
  },
  /**
   * 获取下一天日期
   *
   * @param _year
   * @param _month
   * @param _day
   * @returns {*|{year: number, month: (*|string), day: (*|string), hours: (*|string), minutes: (*|string), seconds: (*|string), week: number, weekDay: *, timeStamp: *}}
   */
  getNextDayFn(_year, _month, _day) {
    // debugger
    const date = _year + '-' + _month + '-' + _day
    const curStamp = this.getStamp(date)
    const oneDatStamp = 24 * 60 * 60 * 1000 // 一天的时间戳
    const nextStamp = curStamp + oneDatStamp
    const nextDate = this.turnDate(nextStamp)

    // console.log(nextDate)
    return nextDate
  }
}

import vuxTool from '@/utils/open/vux-plugin'

/**
   * 识别业务code
   * @param code
   */
/**
 * 获取业务code 转移提醒
 * @param code
 * @returns {boolean}
 */
export function getCodeMsg(data) {
  if (data.code === 0) {
    return true
  } else if (data.code === -1) {
    vuxTool.toast('服务器失联了')
    return false
  } else {
    vuxTool.errorMsg(data.message)
    return false
  }
}

/**
 * 获取响应状态 转义提醒
 * @param code
 * @returns {boolean}
 */
export function getStatusMsg(code) {
  switch (code) {
    case 401:
      vuxTool.errorMsg('登录凭证已失效，请重新登录')
      return false
    case 403:
      vuxTool.errorMsg('您没有操作权限！')
      return false
    case 500:
      vuxTool.errorMsg('服务器出异常！')
      return false
    case 404:
      vuxTool.errorMsg('请求地址找不到！')
      return false
    default:
      return true
  }
}

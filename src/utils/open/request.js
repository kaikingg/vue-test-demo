/**
 * 基于 axios插件 + RESTFUL规范 封装的ajax请求，运用于整个项目
 */
import axios from 'axios'
import store from '@/store/index'
import router from '@/router'
import vuxTool from '@/utils/open/vux-plugin'
import common from '@/utils/open/common'

/**
 * 请求前置处理
 */
axios.defaults.timeout = 30000
axios.interceptors.request.use(config => {
  // store.dispatch('sys/UpdateIsLoading', true)
  config.headers['accept'] = 'application/json; charset=utf-8'
  config.headers['authorization'] = common.getToken()
  return config
}, function(err) {
  vuxTool.hideLoading()
  return Promise.reject(err)
})
/**
 * 请求后置处理
 */
axios.interceptors.response.use(res => {
  vuxTool.hideLoading()
  // debugger
  // setTimeout(() => {
  store.dispatch('sys/UpdateIsLoading', false)
  // }, 2000)
  if (res.status === 200) {
    if (!getCodeMsg(res.data)) {
      return Promise.reject()
    }
  }
  vuxTool.hideLoading()
  return res.data
}, (err) => {
  // console.log('err:', err)
  vuxTool.hideLoading()
  // debugger
  const errMsg = err.toString()
  const code = errMsg.substr(errMsg.indexOf('code') + 5)
  if (!getStatusMsg(parseInt(code))) {
    if (parseInt(code) === 401) {
      common.clearAccountFn() // 清空用户账户数据后重新登录
      // store.dispatch('FedLogOut').then(() => {
      // router.push({ name: 'Login' })
      // })
    }
  }
  return Promise.reject()
})

export default {
  /**
   * 小文件下载方法
   * @param url
   * @param params
   * @param fileName  下载文件的名称，包括后缀
   * @returns {*|Promise<any>}
   */
  download(url, params, fileName) {
    const headers = {
      'content-type': 'application/x-www-form-urlencoded',
      'responseType': 'blob'
    }
    params.fileName = fileName
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        baseURL: url,
        params: params,
        headers: headers,
        responseType: 'blob'
      }).then((res) => {
        resolve(res)
      }).catch((err) => {
        reject(err)
      })
    })
  },

  /**
   * GET 资源请求
   * 特别用于查询操作
   * @param url
   * @param params
   * @returns {*}
   */
  get(url, params) {
    const headers = {
      'content-type': 'application/x-www-form-urlencoded'
    }
    return this.createForm(url, 'get', headers, params)
  },
  /**
   * DELETE 资源请求
   * 特用于删除操作
   * @param url
   * @param params
   * @returns {*}
   */
  delete(url, params) {
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
    return this.createForm(url, 'delete', headers, params)
  },
  /**
   * POST 资源请求
   * 特用于保存操作，支持DTO
   * @param url
   * @param params
   * @returns {*}
   */
  postJson(url, params) {
    const headers = {
      'Content-Type': 'application/json; charset=utf-8'
    }
    params = JSON.stringify(params)
    return this.createJson(url, 'post', headers, params)
  },
  /**
   * POST 资源请求
   * 特用于保存操作，支持FORM参数提交
   * @param url
   * @param params
   * @returns {*}
   */
  postForm(url, params) {
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
    return this.createForm(url, 'post', headers, params)
  },
  /**
   * PUT 资源请求
   * 特用于保存操作，支持DTO
   * @param url
   * @param params
   * @returns {*}
   */
  putJson(url, params) {
    const headers = {
      'Content-Type': 'application/json; charset=utf-8'
    }
    params = JSON.stringify(params)
    return this.createJson(url, 'put', headers, params)
  },
  /**
   * PUT 资源请求
   * 特用于保存操作，支持FORM参数提交
   * @param url
   * @param params
   * @returns {*}
   */
  putForm(url, params) {
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
    return this.createForm(url, 'put', headers, params)
  },
  /**
   * 原生请求 支持FORM表单参数提交
   * @param url
   * @param method
   * @param headers
   * @param params
   * @returns {Promise<any>}
   */
  createForm(url, method, headers, params) {
    headers = headers || {}
    return new Promise((resolve, reject) => {
      axios({
        method: method,
        baseURL: url,
        params: params,
        headers: headers
      }).then((res) => {
        resolve(res)
      }).catch((err) => {
        reject(err)
      })
    })
  },
  /**
   * 原生请求 支持DTO 提交
   * @param url
   * @param method
   * @param headers
   * @param params
   * @returns {Promise<any>}
   */
  createJson(url, method, headers, params) {
    return new Promise((resolve, reject) => {
      // console.log('axios',axios)
      axios({
        method: method,
        baseURL: url,
        data: params,
        headers: headers
      }).then((res) => {
        resolve(res)
      }).catch((err) => {
        reject(err)
      })
    })
  }
}

/**
 * 获取业务code 转移提醒
 * @param code
 * @returns {boolean}
 */
function getCodeMsg(data) {
  if (data.code === 0) {
    return true
  } if (data.code === 310) { // 在线报名-确认报名返回 310 已满员可登记预备
    return true
  } else if (data.code === 548) { // 重复操作
    return true
  } else if (data.code === 30000) { // 重复操作
    return true
  } else if (data.code === -1) {
    vuxTool.toast('服务器失联了')
    return false
  } else if (data.code === 508) {
    // vuxTool.errorMsg('服务器失联了！')
    // initTurnNoData()
    router.replace({ name: 'NotData' })
    return false
  } else {
    vuxTool.errorMsg(data.message)
    return false
  }
}

/**
 * code 508
 * 跳转前 保存 url 参数
 */
function initTurnNoData() {
  let str = location.hash
  let index = str.indexOf('?')
  if (index > -1) {
    str = str.substring(++index, str.length)
    var arr = str.split('&')
    var params = {}
    for (var k of arr) {
      const obj = k.split('=')
      params[obj[0]] = obj[1]
    }
  }
  const routerName = common.getKeyVal('sys', 'prevRouterName')
  common.setKeyVal('sys', 'UpdatePrevRouterObj', { name: routerName, query: params }, 'sessionStorage')
}
/**
   * 获取响应状态 转义提醒
   * @param code
   * @returns {boolean}
   */
function getStatusMsg(code) {
  switch (code) {
    case 401:
      vuxTool.toast('登录凭证已失效，请重新登录')
      return false
    case 403:
      vuxTool.toast('您没有操作权限！')
      return false
    case 500:
      vuxTool.toast('服务器出异常！')
      return false
    case 404:
      vuxTool.toast('请求地址找不到！')
      return false
    default:
      return true
  }
}

/**
 * 基于微信jssdk提供的api，包含图片上传，定位，公众号权限等
 */

/* eslint-disable */
import store from '@/store'
import request from '@/utils/open/request'
import common from '@/utils/open/common'
import constant from '@/constant'
import validate from '@/utils/open/validate'
import vuxTool from '@/utils/open/vux-plugin'

export default{
  config(fn) { // wx.config  微信注入权限验证配置
    const urlparam = common.getKeyVal('user', 'firstUrlParam', 'sessionStorage') || {}
    if(urlparam && !urlparam.appId){
      return
    }
    // console.log('config-urlparam:',urlparam)
    let url1 = location.href.split('#')[0]
    url1 = encodeURIComponent(url1)
    const params = {
      appId: urlparam.appId,
      url: url1
    }
    const url = constant.ctx + 'wechat/api/v1/handler/config'

    request.get(url, params).then((data) => {
      if(data.code === 0) {
        wx.config({
          debug: false,
          appId: urlparam.appId,
          timestamp: data.result.timestamp,
          nonceStr: data.result.nonceStr,
          signature: data.result.signature,
          jsApiList: [
            'hideMenuItems', 'chooseImage', 'previewImage', 'uploadImage', 'downloadImage','updateAppMessageShareData','scanQRCode','getLocation','openLocation','closeWindow',
            'updateTimelineShareData','onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareWeibo', 'onMenuShareQZone' ]
        })

        wx.ready(function() {
          wx.hideMenuItems({ // 要隐藏的菜单项，只能隐藏“传播类”和“保护类”按钮，所有menu项见附录3
            menuList: [
              // 'menuItem:share:appMessage', // 发送给朋友
              // 'menuItem:share:timeline', // 分享到朋友圈
              // 'menuItem:share:qq', // 分享到QQ
              // 'menuItem:share:weiboApp', // 分享到Weibo
              // 'menuItem:share:facebook', // 分享到FB
              // 'menuItem:share:QZone', // 分享到 QQ 空间
              // 'menuItem:copyUrl', // 复制链接
              // 'menuItem:openWithSafari', // 在Safari中打开
              // 'menuItem:share:email', // 邮件
              // 'menuItem:share:brand', // 一些特殊公众号
              // 'menuItem:openWithQQBrowser', // 在QQ浏览器中打开
              // 'menuItem:originPage', // 原网页
              // 'menuItem:refresh' // 刷新
            ]
          })
          console.log('微信验证成功')
          store.dispatch('sys/UpdateIsWechat', true)
          if (!validate.isNull(fn)) {
            fn()
          }
        })
        wx.error(function(res) {
          // console.log('微信验证失败', res)
          store.dispatch('sys/UpdateIsWechat', false) // 微信验证失败后上传组件替换FILE
        })
      }
    })
  },
  hide() {
    console.log(store)
    store.dispatch('sys/UpdateIsShareMaskShow', false)
  },
  share(params, fn) {
    const _this = this
    console.log(params, 'params')
    // wx.ready(function() {
      // 分享给朋友
      wx.updateAppMessageShareData({
        title: params.title, // 分享标题
        desc: params.desc, // 分享描述
        link: params.link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
        imgUrl: params.imgUrl, // 分享图标
        success: function () {
          if (fn) {
            fn.success()
          } else {
            _this.hide()
          }
          // 设置成功
        }
      })
      wx.updateTimelineShareData({
        title: params.title, // 分享标题
        link: params.link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
        imgUrl: params.imgUrl, // 分享图标
        success: function () {
          // 设置成功
          if (fn) {
            fn.success()
          } else {
            _this.hide()
          }
        }
      })
      // wx.onMenuShareTimeline({
      //   title: params.title, // 分享标题
      //   link: params.link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
      //   imgUrl: params.imgUrl, // 分享图标
      //   success: function () {
      //     if(fn) {
      //       fn.success()
      //     }
      //     // 用户点击了分享后执行的回调函数
      //   }
      // })
      // wx.onMenuShareAppMessage({
      //   title: params.title, // 分享标题
      //   desc: params.desc, // 分享描述
      //   link: params.link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
      //   imgUrl: params.imgUrl, // 分享图标
      //   type: params.type, // 分享类型,music、video或link，不填默认为link
      //   dataUrl: params.dataUrl, // 如果type是music或video，则要提供数据链接，默认为空
      //   success: function () {
      //     if(fn) {
      //       fn.success()
      //     }
      //     // 用户点击了分享后执行的回调函数
      //   }
      // });
      wx.onMenuShareQQ({
        title: params.title, // 分享标题
        desc: params.desc, // 分享描述
        link: params.link, // 分享链接
        imgUrl: params.imgUrl, // 分享图标
        success: function () {
          if (fn) {
            fn.success()
          } else {
            _this.hide()
          }
          // 用户确认分享后执行的回调函数
        },
        cancel: function () {
          if(fn) {
            fn.cancel()
          } else {
            _this.hide()
          }
          // 用户取消分享后执行的回调函数
        }
      });
      wx.onMenuShareWeibo({
        title: params.title, // 分享标题
        desc: params.desc, // 分享描述
        link: params.link, // 分享链接
        imgUrl: params.imgUrl, // 分享图标
        success: function () {
          if(fn) {
            fn.success()
          } else {
            _this.hide()
          }
          // 用户确认分享后执行的回调函数
        },
        cancel: function () {
          if(fn) {
            fn.cancel()
          } else {
            _this.hide()
          }
          // 用户取消分享后执行的回调函数
        }
      });
      wx.onMenuShareQZone({
        title: params.title, // 分享标题
        desc: params.desc, // 分享描述
        link: params.link, // 分享链接
        imgUrl: params.imgUrl, // 分享图标
        success: function () {
          if(fn) {
            fn.success()
          } else {
            _this.hide()
          }
          // 用户确认分享后执行的回调函数
        },
        cancel: function () {
          // 用户取消分享后执行的回调函数
          if(fn) {
            fn.cancel()
          } else {
            _this.hide()
          }
        }
      });
    // })
  },
  /**
   * 选择图片
   * @param that
   * @param params
   * @param fn
   */
  chooseImage(that, params, fn) {
    params.testTxt += JSON.stringify(wx) + '\n'
    params.testTxt += '触发了 chooseImage \n '

    params.serverIds = []
    params.imagesid = []
    params.turnImgList = []
    params.loadCount = params.loadCount || 9
    const _this = this
    let isShowLoading = true
    that.isAllowUpLoad = true
    console.log('开始选择图片，params',params)
    wx.chooseImage({
      count: 9, // 默认9
      sizeType: ['original'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function(res) {
        if (isShowLoading) {
          isShowLoading = false
        }
        params.testTxt += ' wx.chooseImage 成功 \n'
        params.testTxt += res.localIds + '  \n'
        const len = res.localIds.length
        params.localIds = res.localIds // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
        console.log(res.localIds, '返回的本地ID列表')
        store.dispatch('upload/UpdateIsUploadImage', true)
        that.$emit('uploadSuceess', false)
        _this.syncUpload(that, params, len, (res) => {
          if (!validate.isNull(fn)) {
            fn(res)
          }
        })
      },
      fail() {
        console.log('调用失败')
      },
      complete() {
        // alert('chooseImage调用完成执行 complete ')

      }
    })
  },
  syncUpload(that, params, len,  fn) {
    const _this = this
    const localId = params.localIds.pop() // 每轮询完一张就删掉一张
    const _str = '正在上传照片'+(len- (params.localIds.length)) +'/'+len +'张'
    store.dispatch('upload/UpdateIsUploadImageTxt', _str)
    console.log('syncUpload-长度', len, params.localIds.length)
    wx.uploadImage({
      localId: localId,
      isShowProgressTips: 1,
      success: function(res) {
        console.log('wx.uploadImage-success', res)
        const serverId = res.serverId // 返回图片的服务器端ID
        params.testTxt += ' wx.uploadImage成功返回图片 \n '
        params.testTxt += ' : ' + serverId + '\n'
        params.serverIds.push(serverId)
        const urlparam = common.getKeyVal('user', 'firstUrlParam')
        const paramObj = {
          mediaId: serverId,
          appId: urlparam.appId
        }
        _this.uploadImg({
          params: paramObj,
          methods: 'get',
          callback(res) {
            params.turnImgList = [res]
            if (res.code === 0) {
              console.log('_this.uploadImg', res)
              if (params.localIds.length === 0) { // 当传完所有的图片，执行回调函数
                vuxTool.hideLoading()
                store.dispatch('upload/UpdateIsUploadImage', false)
                that.$emit('uploadSuceess', true)
                vuxTool.toast('上传图片成功')
              }
              if (!validate.isNull(fn)) {
                fn(params)
              }

              // 处理剩下的
              if (params.localIds.length > 0) {
                _this.syncUpload(that, params, len, fn)
              }
            } else {
              store.dispatch('upload/UpdateIsUploadImage', false)
              vuxTool.warnMsg('上传图片失败')
            }
          },
          // 上传本地服务器失败回调
          errorCallback(error){
            // console.log('上传本地失败', error)
            vuxTool.warnMsg('上传服务器图片失败')
            store.dispatch('upload/UpdateIsUploadImage', false)
            vuxTool.hideLoading()
          }
        })
      },
      fail: function(res) {
        console.log('wx.uploadImage-fail', res)
        vuxTool.warnMsg('微信上传图片失败,请重新上传！')
        params.testTxt += ' wx.uploadImage 失败 '
        store.dispatch('upload/UpdateIsUploadImageTxt', '上传出错，请重新上传')
        setTimeout(() =>{
          store.dispatch('upload/UpdateIsUploadImage', false)
        },300)
        vuxTool.hideLoading()
      }
    })
  },
  /**
   * wx上传图片
   * @param data
   */
  uploadImg(data) {
    const url1 = constant.ctx + 'basics/api/v1/attachment/getmedia'

    request.get(url1, data.params).then((res) => {
      if (data.callback) {
        data.callback(res)
      }
    }).catch((error) => {
      // console.log(error)
      if(data.errorCallback){
        data.errorCallback(error)
      }
    })
  },
  /**
   * 微信扫一扫
   */
  scanCode(fn) {
    wx.scanQRCode({
      needResult: 1, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，
      scanType: ['qrCode'], // 可以指定扫二维码还是一维码，默认二者都有
      success: function(res) {
        const result = res.resultStr // 当needResult 为 1 时，扫码返回的结果
        console.log(result, '******扫描结果****')
        if (fn) {
          fn(res)
        }
      },
      error: function() {
        console.log('123')
      }
    })
  },
  /**
   * 微信获取当前位置
   * params fn
   */
  getLocation(fn) {
    console.log('执行微信获取地理位置')
    wx.getLocation({
      type: 'gcj02', // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
      success: function(res) {
        const latitude = res.latitude // 纬度，浮点数，范围为90 ~ -90
        const longitude = res.longitude // 经度，浮点数，范围为180 ~ -180。
        const speed = res.speed // 速度，以米/每秒计
        const accuracy = res.accuracy // 位置精度
        console.log(res, '***********************')
        if (fn) {
          console.log(fn, '获取成功对数据处理的方法')
          fn(res)
        }
      }
    })
  },
  openLocation(params) {
    const _params = {
      latitude: 0, // 纬度，浮点数，范围为90 ~ -90
      longitude: 0, // 经度，浮点数，范围为180 ~ -180。
      name: '', // 位置名
      address: '', // 地址详情说明
      scale: 26, // 地图缩放级别,整形值,范围从1~28。默认为最大
      infoUrl: '' // 在查看位置界面底部显示的超链接,可点击跳转
    }
    if (params) {
      for(let x in params){
        _params[x] = params[x]
      }
    }
    wx.openLocation(_params);
  },
}
/* eslint-enable */

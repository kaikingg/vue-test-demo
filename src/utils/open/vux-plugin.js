/**
 * 基于vux插件的弹窗交互工具类
 */
import Vue from 'vue'
import validate from '@/utils/open/validate'

// 弹窗组件
import {
  ToastPlugin,
  TransferDom,
  XDialog,
  ConfirmPlugin, AlertPlugin,
  DatetimePlugin, LoadingPlugin,
  Actionsheet } from 'vux'

Vue.use(ToastPlugin, { time: '2500' })
Vue.use(ConfirmPlugin)
Vue.use(AlertPlugin)
Vue.use(DatetimePlugin)
Vue.use(LoadingPlugin)
Vue.use(Actionsheet)
Vue.use(ToastPlugin)
Vue.use(XDialog)
Vue.directive('transfer-dom', TransferDom)

export default {
  loading(msg) {
    // 显示
    Vue.$vux.loading.show({
      text: msg
    })
  },
  hideLoading() {
    // 隐藏
    Vue.$vux.loading.hide()
  },
  confirm(title, content, fn) {
    // if (!Vue.$vux.confirmDom) {
    //   Vue.$vux.confirmDom = document.querySelector('.vux-confirm')
    // }
    // Vue.$vux.confirmDom.style.display = 'block'
    Vue.$vux.confirm.show({
      title: title || '提示',
      content: content,
      onCancel() {
        fn && fn(false)
        // Vue.$vux.confirmDom.style.display = 'none'
      },
      onConfirm() {
        fn && fn(true)
        // Vue.$vux.confirmDom.style.display = 'none'
      }
    })
  },
  /**
   * toast
   * @param {String} msg
   * @param {String} pos 显示位置，可选值 default, top, middle, bottom
   */
  toast(msg, pos) {
    if (!validate.isNull(msg)) {
      pos = pos || 'top'
      Vue.$vux.toast.text(msg, pos)
    }
  },
  /**
   * 隐藏 toast
   */
  hideToast() {
    Vue.$vux.toast.hide()
  },
  /**
   * 成功提示
   * @param {String} msg
   */
  successMsg(msg) {
    Vue.$vux.toast.show({
      text: msg || '成功',
      type: 'success'
    })
  },
  /**
   * 取消提示
   * @param {String} msg
   */
  cancelMsg(msg) {
    Vue.$vux.toast.show({
      text: msg,
      type: 'cancel',
      time: 3000
    })
  },
  /**
   * 错误提示
   * @param {String} msg
   */
  errorMsg(msg) {
    Vue.$vux.alert.show({
      title: '提示',
      content: msg,
      onShow() {
        // console.log('Plugin: I\'m showing')
      },
      onHide() {
        // console.log('Plugin: I\'m hiding')
      }
    })
  },
  /**
   * 警告提示
   * @param {String} msg
   */
  warnMsg(msg) {
    // Vue.$vux.toast.text(msg, 'top')
    Vue.$vux.toast.show({
      text: msg,
      type: 'warn',
      time: 4000
    })
  }
}

/**
 * 以下不再维护，后期会删掉
 */

// toast 弹窗 已注释，将废弃
Vue.prototype.oToast = function(options) {
  // const op = {
  //   showPositionValue: true,
  //   type: 'text', // success, warn, cancel, text
  //   text: '执行成功',
  //   width: '2.7rem',
  //   position: 'middle',
  //   time: 2000
  // }
  // for (const k in options) {
  //   if (k && options.hasOwnProperty(k)) {
  //     op[k] = options[k]
  //   }
  // }
  // this.$vux.toast.show({
  //   showPositionValue: op.showPositionValue,
  //   text: op.text,
  //   type: op.type,
  //   width: op.width,
  //   position: op.position,
  //   time: op.time
  // })
}

/**
 * 以下不再维护，后期会删掉
 */
// confirm 弹窗
Vue.prototype.oConfirm = function(options) {
  const op = {
    showPositionValue: true,
    title: '提示',
    text: '确定要删除？',
    confirmText: '确定',
    calcelText: '取消'
  }
  for (const k in options) {
    if (k && options.hasOwnProperty(k)) {
      op[k] = options[k]
    }
  }
  this.$vux.confirm.show({
    showPositionValue: op.showPositionValue,
    title: op.title,
    content: op.text,
    confirmText: op.confirmText,
    calcelText: op.calcelText,
    onCancel() {
      if (op.fn) {
        op.fn(false)
      }
    },
    onConfirm() {
      if (op.fn) {
        op.fn(true)
      }
    }
  })
  // this.$vux.confirm.show()
}

/**
 * 以下不再维护，后期会删掉
 */
// confirm 弹窗
Vue.prototype.oAlert = function(options) {
  const op = {
    showPositionValue: true,
    title: '提示',
    text: '确定要删除？',
    confirmText: '确定'
  }
  for (const k in options) {
    if (k && options.hasOwnProperty(k)) {
      op[k] = options[k]
    }
  }
  this.$vux.alert.show({
    showPositionValue: op.showPositionValue,
    title: op.title,
    content: op.text,
    confirmText: op.confirmText,
    onShow() {
      // console.log('Plugin: I\'m showing')
      if (op.fn) {
        op.fn(true)
      }
    },
    onHide() {
      // console.log('Plugin: I\'m hiding')
      if (op.fn) {
        op.fn(true)
      }
    }
  })
  // this.$vux.confirm.show()
}


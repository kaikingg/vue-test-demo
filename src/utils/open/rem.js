/**
 * css单位rem相应配置
 */
// let htmlFontSize = '';

// (function (doc, win) {
//   let docEl = doc.documentElement
//   let resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize'
//   let recalc = function () {
//     let clientWidth = docEl.clientWidth
//     if (!clientWidth) return
//     let fontSize = clientWidth / 20
//     docEl.style.fontSize = fontSize + 'px'
//     window.sessionStorage.setItem('htmlFontSize',fontSize)
//   }
//   if (!doc.addEventListener) return
//   win.addEventListener(resizeEvt, recalc, false)
//   doc.addEventListener('DOMContentLoaded', recalc, false)
// })(document, window)

(function() {
  const doc = window.document
  const docEl = doc.documentElement
  let tid
  function refreshRem() {
    let width = docEl.getBoundingClientRect().width
    if (width > 750) {
      width = 750
    }
    const rem = width / 7.5
    docEl.style.fontSize = rem + 'px'
  }

  window.addEventListener('resize', function() {
    clearTimeout(tid)
    tid = setTimeout(refreshRem, 100)
  }, false)

  window.addEventListener('pageshow', function(e) {
    if (e.persisted) {
      clearTimeout(tid)
      tid = setTimeout(refreshRem, 100)
    }
  }, false)

  if (doc.readyState === 'complete') {
    doc.body.style.fontSize = 12 + 'px'
  } else {
    doc.addEventListener('DOMContentLoaded', function() {
      doc.body.style.fontSize = 12 + 'px'
    }, false)
  }

  refreshRem()
}
)()

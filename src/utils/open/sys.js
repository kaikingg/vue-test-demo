import constant from '@/constant'
import store from '@/store/index'
import request from './request'
// import ext from '@/ext'

/**
 * //todo
 * 需要分开迁移到
 */
export default {
  // getDictionaryData(type) { // 获取字典值
  //   console.log('获取-dictionaryData-已注释')
  //   alert('获取-dictionaryData-已注释')
  //   // const dicObj = {
  //   //   askforleaveType: { text: '请假类型', dictType: 'askforleaveType' }, // 请假类型
  //   //   sickLeaveType: { text: '病假类型', dictType: 'sickLeaveType' }, // 病假类型
  //   //   attendanceType: { text: '考勤', dictType: 'attendanceType' }, // attendanceType 1506 出勤， 1507 病假， 1508 事假， 1509 缺勤
  //   //   classtype: { text: '班级类别', dictType: 'classtype', url: 'basics/api/v1/opendictinfo/query/list' }, // 班级类别
  //   //   guardianStudentRelation: { text: '家长与学生关系', dictType: 'guardianStudentRelation' }, // 家长与学生关系
  //   //   sex: { text: '性别', dictType: 'sex' }, // 性别
  //   //   nationType: { text: '国籍', dictType: 'nationType' }, // 国籍
  //   //   nationalType: { text: '民族', dictType: 'nationalType', isOrderBy: '1' }, // 民族 isOrderBy:'1', // isOrderBy 是否按字典项名称首字母拼音排序 1 是 2 否
  //   //   residenceType: { text: '户口性质', dictType: 'residenceType' }, // 户口性质
  //   //   nonAgricultureType: { text: '非农业户口类型', dictType: 'nonAgricultureType' }, // 非农业户口类型
  //   //   isStay: { text: '是否留守儿童 ', dictType: 'isStay' }, // 是否留守儿童
  //   //   bloodType: { text: '血型', dictType: 'bloodType' }, // 血型
  //   //   cardType: { text: '证件类型', dictType: 'cardType' }, // 证件类型
  //   //   health: { text: '健康状况', dictType: 'health' }, // 健康状况
  //   //   disabilityType: { text: '残疾类型', dictType: 'disabilityType' }, // 残疾类型
  //   //   guardianCardType: { text: '监护人证件类型', dictType: 'guardianCardType' } // 监护人证件类型
  //   // }

  //   // const params = dicObj[type]
  //   // return new Promise((resolve, reject) => {
  //   //   const typeName = params.dictType
  //   //   const data = constant.dictionaryData
  //   //   if (!data[typeName]) {
  //   //     // const newData = JSON.parse(window.sessionStorage.getItem('dictionaryData')) || {}
  //   //     const newData = store.state.sys.dictionaryData
  //   //     if (!newData[typeName]) {
  //   //       const params1 = {
  //   //         dictType: '', // type
  //   //         isOrderBy: '2', // isOrderBy 是否按字典项名称首字母拼音排序 1 是 2 否
  //   //         type: 1419 // 类型 1 系统 2 业务
  //   //       }
  //   //       for (const key in params1) {
  //   //         if (key && params1.hasOwnProperty(key)) {
  //   //           params1[key] = params[key]
  //   //         }
  //   //       }
  //   //       ext.sys.getDictionaryData(params1, params1.dictType).then(res => {
  //   //         resolve(res)
  //   //       })
  //   //     } else {
  //   //       constant.dictionaryData[typeName] = newData[typeName]
  //   //       resolve(newData[typeName])
  //   //     }
  //   //   } else {
  //   //     resolve(data[typeName])
  //   //   }
  //   // })
  // },
  getChatRecord() { // 获取聊天未读信息数
    const params = {
      userId: store.state.user.curRole.relationId, // 角色id
      userType: store.state.user.curRole.identityType, // 身份类型 id
      size: 10000, // 条数
      number: 1, // 页码
      sort: 'update_time', // 按创建时间排序
      direction: 'desc' // 降序
    }
    const url = constant.ctx + 'manage/api/v1/yschatmain/query/unreadCount'
    request.get(url, params).then((res) => {
      if (res.code === 0) {
        constant.infoTotal.chat = 0
        constant.newsNumFirst['chat'] = res.result.unreadCount
        this.caculatMailNum() // 计算消息未读消息 总数
      }
    })
  },
  getDynamicNum() { // 获取 动态消息 未读数量
    return new Promise((resolve, reject) => {
      const params = {
        userId: store.state.user.userInfo.id, // 用户身份id
        typeId: store.state.user.curSelectRole.identityType
      }
      // debugger
      const url = constant.ctx + 'manage/api/v2/dynamic/query/dynamic/count'
      request.get(url, params).then((res) => {
        if (res.code === 0) {
          const data = res.result
          const newsNumFirst = constant.newsNumFirst
          for (const key in data) {
            if (key && data.hasOwnProperty(key)) {
              newsNumFirst[key] = parseInt(data[key])
            }
          }
          this.caculatDynamicNum() // 计算动态未读消息 总数
          resolve()
        } else {
          reject()
        }
      })
    })
  },
  caculatDynamicNum() { // 计算动态未读消息 总数
    const infoNumObj = constant.infoTotal
    const newsNumObj = constant.newsNumFirst
    let total1 = 0
    let total2 = 0
    for (const k in newsNumObj) {
      if (k && newsNumObj.hasOwnProperty(k)) {
        total1 += newsNumObj[k]
      }
    }

    for (const k in infoNumObj) {
      if (k && infoNumObj.hasOwnProperty(k)) {
        total2 += infoNumObj[k]
      }
    }

    const total = total1 + total2
    store.dispatch('news/updateDynamicInfos', total)
  },
  caculatMailNum(unreadNum) { // 计算消息未读消息 总数
    unreadNum = unreadNum || 0
    if (unreadNum > 0) { // unreadNum 本次阅读几条，就消减几条未读数
      unreadNum = constant.newsNumFirst.chat - unreadNum
      if (unreadNum > 0) { // 没剩了
        constant.newsNumFirst.chat = unreadNum
      } else if (unreadNum < 0) { // 还剩
        constant.infoTotal.chat = constant.infoTotal.chat - Math.abs(unreadNum)
      }
    }

    const total = constant.infoTotal.chat + constant.newsNumFirst.chat
    store.dispatch('news/updateMailInfos', total)
  }
}

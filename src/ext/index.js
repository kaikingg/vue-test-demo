import etl from './modules/etl'
import kg from './modules/kg'
import sys from './modules/sys'
import user from './modules/user'
import wechat from './modules/wechat'
import pay from './modules/pay'
import recruit from './modules/recruit'

/**
 * 框架扩展目录，主要放置 数据接口扩展逻辑，针对接口做ETL或者封装，隔离接口
 * @type {{etl: {}}}
 */
const ext = {
  etl,
  kg,
  sys,
  user,
  wechat,
  recruit,
  pay
}
export default ext

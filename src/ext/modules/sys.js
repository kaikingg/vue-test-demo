import api from '@/api'
import store from '@/store'
import constant from '@/constant'

/**
 * 针对获取数据进行二次数据清洗、加工、转化，特别指API
 */
export default {
  /**
   * 字典数据查询 将弃用
   * @param {Object} paramsData
   */
  // getDictionaryData(params, typeName) {
  //   // return new Promise((resolve, reject) => {
  //   //   this.getDicData(typeName).then(res => {
  //   //     resolve(res)
  //   //   }).catch(e => {
  //   //     reject()
  //   //   })
  //   // })
  // },
  /**
   * 字典数据查询
   * @param {String} dictType 字典类型
   * @param {String} isOrderBy
   */
  getDicData(dictType, type, isOrderBy) {
    return new Promise((resolve, reject) => {
      // debugger
      api.sys.getDicData(dictType, type, isOrderBy).then((res) => {
        const data = store.state.sys.dictionaryData
        data[dictType] = res
        store.dispatch('UpdateDictionaryData', data)
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  },
  /**
   * 获取首页banner图
   */
  getBannerList() {
    return new Promise((resolve, reject) => {
      api.sys.getBannerList().then(res => {
        // debugger
        if (res.code === 0 && res.result.length > 0) {
          for (const obj of res.result) {
            obj.img = constant.file_ctx + '' + obj.image
          }
          resolve(res)
        } else {
          reject()
        }
      }).catch(e => {
        reject(e)
      })
    })
  }
}

import common from '@/utils/open/common'
import api from '@/api'
import store from '@/store'

/**
 * 针对获取数据进行二次数据清洗、加工、转化，特别指API
 */
export default {
  getWechatInfo() {
    return new Promise((resolve, reject) => {
      api.user.getWechatInfo().then((weChatInfo) => {
        // console.log('store.state.user.curSelectUnit.openId', store.state.user.curSelectUnit)
        const isBind = !!store.state.user.curSelectUnit.openId
        weChatInfo.nickNameTip = weChatInfo.nickName
        weChatInfo.headImgUrlTip = weChatInfo.headImgUrl
        if (!isBind) {
          weChatInfo.nickName = ''
          weChatInfo.headImgUrl = ''
        }
        weChatInfo.isBind = isBind // 通过 openId 判断是否已绑定
        weChatInfo.isShow = true
        common.setKeyVal('user', 'weChatInfo', weChatInfo)
        resolve(weChatInfo)
      }).catch(error => {
        reject(error)
      })
    })
  }
}

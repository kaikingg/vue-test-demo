// import common from '@/utils/open/common'
import api from '@/api'
import $vuxTool from '@/utils/open/vux-plugin'

/**
 * 针对获取数据进行二次数据清洗、加工、转化，特别指API
 */
export default {
  /**
   * @param params {
   * body: 订单货品名称
   * outTradeNo: 订单编号
   * totalFee: 总金额
   * appId: 微信服务号ID
   * openId:  微信用户ID
   * }
   * @param resolve
   * @param reject
   */
  wechatJsapi(params, resolve, reject) {
    $vuxTool.loading('操作中')
    if (typeof WeixinJSBridge === 'undefined') {
      $vuxTool.toast('当前环境不支持微信支付')
      return
    }
    api.pay.wechatUnifiedorder({
      body: params.body,
      outTradeNo: params.outTradeNo,
      total_fee: params.totalFee,
      openid: params.openId,
      appid: params.appId
    }, (res) => {
      const r = res.result
      WeixinJSBridge.invoke(
        'getBrandWCPayRequest', {
          'appId': params.appId, // 公众号名称，由商户传入
          'timeStamp': r.timeStamp + '', // 时间戳，自1970年以来的秒数
          'nonceStr': r.nonceStr, // 随机串
          'package': r.package,
          'signType': 'MD5', // 微信签名方式：
          'paySign': r.paySign // 微信签名
        },
        function(res) {
          console.log('WeixinJSBridge function:' + res)
          if (res.err_msg === 'get_brand_wcpay_request:ok') {
            api.pay.wechatOrderquery({ outTradeNo: params.outTradeNo, appid: params.appId }, (res) => {
              if (res.result.code === 'SUCCESS') {
                resolve(res)
              } else {
                reject(res)
              }
            }, (res) => {
              console.log('微信支付查询失败：wechatOrderquery', 1500)
              reject(res)
            })
          } else {
            reject(res)
          }
        })
      $vuxTool.hideLoading()
    }, (res) => {
      $vuxTool.hideLoading()
      this.$vuxTool.toast('订单接口异常，请重试')
      reject(res)
    })
  }
}

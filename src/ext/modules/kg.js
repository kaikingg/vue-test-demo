// import request from '@/utils/open/request'
import common from '@/utils/open/common'
import api from '@/api/index'
import constant from '@/constant'
import timePlugin from '@/utils/open/time-plugin'
import store from '@/store'

/**
 * 针对获取数据进行二次数据清洗、加工、转化，特别指API
 */
export default {
  /**
   * 作业数据处理
   */
  getHomeworkDetailData(res) { // 作业数据处理
    return new Promise((resolve, reject) => {
      try {
        res.result = res.result || {}
        const data = res.result.ysHomework || {}

        // 获取当前时间戳
        const date = new Date()
        const curTimetamp = date.getTime()

        if (data.planFinishTime) {
          const planFinishDate = timePlugin.turnDate(data.planFinishTime)
          data.planFinishTime1 = planFinishDate.year + '-' + planFinishDate.month + '-' + planFinishDate.day
        }
        if (data.createTime) {
          const createDate = timePlugin.formatDate(data.createTime, curTimetamp)
          data.createTime1 = createDate
        }
        data.swiperDefaultList = []
        for (const k of data.attachmentList) {
          if (k && k.filePath) {
            k.filePath = common.cutThumbnailSmall(k.filePath) // 图片切换成缩略图显示
            const obj = { img: constant.$imgUrl + k.filePath, id: k.filePath }
            data.swiperDefaultList.push(obj)
          }
        }
        data.homeworkMsg = common.setCharactersTextImg(data.homeworkMsg) // 设置显示特殊字符
        resolve(data)
      } catch (e) {
        reject()
      }
    })
  },
  /**
   * 获取 招生活动 数据
   */
  getRecruitData(params) {
    return new Promise((resolve, reject) => {
      if (!params) {
        params = {
          type: 1 // 活动类型 类型 1.常规 2.季度 现在默认提交类型为1
        }
      }
      api.kg.getRecruitData(params).then((res) => {
        store.dispatch('UpdateRecruitData', res)
        resolve(res)
      }).catch(e => {
        store.dispatch('UpdateRecruitData', {})
        reject(e)
      })
    })
  }
}

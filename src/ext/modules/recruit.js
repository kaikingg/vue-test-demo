import api from '@/api'
// import constant from '@/constant'

export default {
  getRecruitStudentDetail(item) {
    return new Promise((resolve, reject) => {
      // debugger
      api.recruit.getRecruitStudentDetailData(item).then(res => {
        resolve(res)
      }).catch(e => {
        reject(e)
      })
    })
  }
}

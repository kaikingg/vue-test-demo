let firstUrlParamDefault
// firstUrlParamDefault = {openId: 'omZ3l1CBXMvoZIFfMENB3crRkBvY', appId: 'wx28ab23326afd6008', unitId: '1'} // 李杏娟 + 家长 启动页 url 传参默认参数
firstUrlParamDefault = { openId: 'oqR0b1T102hug7VZQrkyIdrzm3ug', appId: 'wx28ab23326afd6008', unitId: '1' } // 刘婉婷 classId: "34" 老师 启动页 url 传参默认参数
// firstUrlParamDefault = {openId: 'omZ3l1O7rkaIICN6pdcJ45iFIW20', appId: 'wx28ab23326afd6008', unitId: '261993644112740352'} // 彭锦芳 classId: "34" 老师 启动页 url 传参默认参数
// firstUrlParamDefault = {openId: 'oqR0b1VUoYUOomD9O2jgSBVf_DVA', appId: 'wx28ab23326afd6008', unitId: '261993644112740352'} // 家长黄杰锋  classId: "34" 启动页 url 传参默认参数
// firstUrlParamDefault = {openId: 'oqR0b1bjLCmPX4zZYJdKNBetKvNw', appId: 'wx28ab23326afd6008', unitId: '261993644112740352'} // 园长 启动页 url 传参默认参数
// firstUrlParamDefault = {openId: 'omZ3l1M83kL2EfnjZNXzDtTeBKGc', appId: 'wx28ab23326afd6008', unitId: '261993644112740352'}  // 李翠霞 园长 启动页 url 传参默认参数
// firstUrlParamDefault = {openId: 'oO70Mw3mGzhCF5xY7V43ZkNGRGVs', appId: 'wx28ab23326afd6008', unitId: '261993644112740352'} // 罗华 老师 全员考勤 启动页 url 传参默认参数
// firstUrlParamDefault = {openId: 'oO70MwywWtF7Ko6zYimXYUKj54TA', appId: 'wx28ab23326afd6008', unitId: '261993644112740352'} // 中班老师 启动页 url 传参默认参数
// firstUrlParamDefault = {openId: 'oO70MwxCvPd8X02sFVX8tqKDvx7o', appId: 'wx28ab23326afd6008', unitId: '261993644112740352'} // 小班老师 启动页 url 传参默认参数
// firstUrlParamDefault = {openId: 'oqR0b1T102hug7VZQrkyIdrzm3ug', appId: 'wx28ab23326afd6008', unitId: '261993644112740352'} // 普通 启动页 url 传参默认参数
// firstUrlParamDefault = {openId: 'oO70Mw_qnTSLmBCXYOCt2uV8P4rw', appId: 'wx28ab23326afd6008', unitId: '1'} // 普通 启动页 url 传参默认参数
// firstUrlParamDefault = {openId: 'omZ3l1DLNOVc9YmH6H_lnOeHdhkY', appId: 'wx2cd3a98f3ec208a7', unitId: '261993644112740352'} // 潘锦发（员工：大二班老师）
// firstUrlParamDefault = {openId: 'omZ3l1LR3kP1EVeZn94RGU_n65YM', appId: 'wx2cd3a98f3ec208a7', unitId: '261993644112740352'} // ios测试机 （员工，家长：大二班学生）
firstUrlParamDefault = { openId: 'omZ3l1F2-tNO21hECPVVK4DjZFng', appId: 'wx2cd3a98f3ec208a7', unitId: '261993644112740352' } // 谢成寿 （员工）
// firstUrlParamDefault = {openId: 'omZ3l1CBXMvoZIFfMENB3crRkBvY', appId: 'wx2cd3a98f3ec208a7', unitId: '261993644112740352'} // 钟志伟（员工）

export default firstUrlParamDefault

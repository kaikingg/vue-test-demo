/**
 * 系统常量脚本
 */
// import accountNumber from './account-number'

export default {
  /**
   * 默认头像
   */
  // headPortraitImg: '../../static/img/parent/icon-others.png',
  /**
   * 新增缩略图配置
   * 261993644112740352/78cc384c96614d50bd4c0c81786c9aa5.jpg?x-oss-process=image/auto-orient,1/resize,p_50/quality,q_90
   * http://dev-oss.lezhide.cn/261993644112740352/d8eacf8179ee41cabee8f6ea4c17ab8b.jpg?x-oss-process=image/auto-orient,1/resize,p_80/quality,q_90
   * x-oss-process=image/auto-orient,1/resize,p_80/quality,q_90
   */
  thumbnail: {
    smaller: '?x-oss-process=image/resize,p_20/quality,q_90',
    small: '?x-oss-process=image/resize,p_50/quality,q_90',
    large: '?x-oss-process=image/resize,p_80/quality,q_90'
  },
  /**
   * 支持上传视频格式
   * h5 video 只支持播放 Ogg MPEG4 WebM 这三种格式
   * swf|avi|flv|mpg|rm|mov|wav|asf|3gp|mkv|rmvb|wmv|mp4|ogg
   * WebM|MPEG4|mp4|ogg|mov
   */
  // videoReg: 'WebM|MPEG4|mp4|ogg|mov',
  videoReg: new RegExp('\.(WebM|MPEG4|mp4|ogg|mov)$', 'i'),
  /**
   * 环境配置
   */
  ctx: process.env.CTX,
  /**
   * 分享配置
   */
  domain_ctx: process.env.DOMAIN_CTX,
  /**
   * 文件访问地址
   */
  file_ctx: process.env.FILE_CTX,
  /**
   * 是否开启调试面板
   */
  isDebug: process.env.DEBUG,
  orderSetting: process.env.ORDER_SETTING,
  /**
   * websocket连接地址
   */
  ws_ctx: process.env.WS_CTX,
  /**
   * apk
   */
  apk: 'Vb8Uk9N&Q2*zDbx$',
  /**
   * 测试账号
   */
  // firstUrlParamDefault: accountNumber,
  /**
   * 开发功能手机号 todo 测试使用
   */
  // permitPhoneArr: ['18688885995', '13570933988', '13926480909',
  //   '13826368045', '13431004467', '18125902219',
  //   '18022389052', '13189019277', '13622896620'],
  /**
   * 消息列表 接受的数据
   */
  dictionaryData: [], // 已不再使用
  mailNewsRecord: [],
  chatRecordStore: [], // 发送数据 数组
  chatRecordReceive: [], // 接收
  /**
   * 消息数量处理(动态变化)
   */
  infoTotal: {
    chat: 0,
    leave: 0,
    attendance: 0,
    homework: 0,
    notice: 0,
    album: 0
  },
  /**
   * 消息数量处理(进入首页获取的，固定值，每次首页刷新获取一次)
   */
  newsNumFirst: {
    chat: 0,
    leave: 0,
    attendance: 0,
    homework: 0,
    notice: 0,
    album: 0
  },
  /**
   * 选择发送通知对象
   */
  noticeTarget: [
    { name: '全园', id: 1 },
    { name: '全体员工', id: 2 },
    { name: '全体家长', id: 3 },
    { name: '定向', id: 4 }
  ],
  /**
   * 性别 字典值处理
   * 1538 ：家长端;  1537 : 教师端
   */
  sexObj: {
    '1538-1428': {
      key: '男',
      code: '1428',
      icon: 'protrait-boy',
      imgUrl: '/static/img/default/icon-baby-boy.png'
    },
    '1538-1429': {
      key: '女',
      code: '1429',
      icon: 'protrait-gril',
      imgUrl: '/static/img/default/icon-baby-girl.png'
    },
    '1537-1428': {
      key: '男',
      code: '1428',
      icon: 'protrait-boy',
      imgUrl: '/static/img/default/icon-male-teacher.png'
    },
    '1537-1429': {
      key: '女',
      code: '1429',
      icon: 'protrait-gril',
      imgUrl: '/static/img/default/icon-female-teacher.png'
    },
    'other': {
      key: '其他',
      code: '',
      icon: 'protrait-other',
      imgUrl: '/static/img/svg/icon-tourist.svg'
    }
  },
  /**
   * 登录接口
   */
  sys: '',
  /**
   * 登出接口
   */
  loginOutApi: '',

  /**
   * 获取用户信息接口
   */
  userInfoApi: 'auth/api/v1/pc/sysuser/query/info',
  /**
   * 主题颜色
   */
  theme: {
    bgColor: '#F7F7F7',
    mainColor: '#f85d4c',
    borderColor: '#F4F4F4',
    primaryColor: '#5aa7f9',
    successColor: '#73c841',
    infoColor: '#909399',
    warningColor: '#e6a23c',
    dangerColor: '#f85d4c',
    calcelColor: '#ddd'
  }
}

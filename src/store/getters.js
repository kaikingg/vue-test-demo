const getters = {
  tabbar: state => state.tabbar,
  token: state => state.user.token,
  userInfo: state => state.user.userInfo,
  user: state => state.user,
  curRole: state => state.user.curRole,
  curSelectRole: state => state.user.curSelectRole,
  permission: state => state.user.permission,
  unitList: state => state.user.unitList,
  unreadNum: state => state.user.unreadNum,
  unreadList: state => state.user.unreadList,
  imgList: state => state.user.imgList
}
export default getters

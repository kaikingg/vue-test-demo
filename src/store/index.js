import Vue from 'vue'
import Vuex from 'vuex'

import sys from './modules/sys'
import kg from './modules/kg'
import navigation from './modules/navigation'
import news from './modules/news'
import notice from './modules/notice'
import homework from './modules/homework'
import album from './modules/album'
import swiper from './modules/swiper'
import user from './modules/user'
import tabbar from './modules/tabbar'
import vuxTool from './modules/vuxTool'
import campus from './modules/campus'
import upload from './modules/upload'
import getters from './getters'
import file from './modules/file'
import comment from './modules/comment'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    kg,
    sys,
    navigation,
    user,
    news,
    notice,
    homework,
    album,
    swiper,
    vuxTool,
    upload,
    tabbar,
    campus,
    file,
    comment
  },
  getters
})

// const shouldUseTransition = !/transition=none/.test(location.href)
// store.registerModule('vux', {
//   state: {
//     demoScrollTop: 0
//   },
//   mutations: {
//     updateDemoPosition(state, payload) {
//       state.demoScrollTop = payload.top
//     }
//   },
//   actions: {
//     updateDemoPosition({ commit }, top) {
//       commit({ type: 'updateDemoPosition', top: top })
//     }
//   }
// })

// export default store

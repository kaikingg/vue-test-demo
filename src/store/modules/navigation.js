/**
 * 头部导航
 */

import common from '@/utils/open/common'
import validate from '@/utils/open/validate'

const navigation = {
  namespaced: true,
  state: {
    headerObj: { // 页面标题
      isShow: true, // 默认 true
      title: '加载中',
      headerStyle: {},
      clickEvent: () => {},
      optionsL: {
        isShow: true, // 默认 true
        isHtml: false,
        text: '',
        iconStyle: {},
        icon: 'icon-fanhui',
        clickEvent: ''
      },
      optionsR: {
        isShow: false, // 默认 false
        text: '',
        iconStyle: {},
        icon: 'icon-gengduoxuanxiang',
        clickEvent: () => {}
      }
    },
    /**
     * 备份重置
     */
    headerObjCopy: { // 页面标题
      isShow: true, // 默认 true
      title: '幼儿园',
      headerStyle: {},
      optionsL: {
        isShow: true, // 默认 true
        isHtml: false,
        text: '',
        iconStyle: {},
        icon: 'icon-fanhui',
        clickEvent: ''
      },
      optionsR: {
        isShow: false, // 默认 false
        text: '',
        iconStyle: {},
        icon: 'icon-gengduoxuanxiang',
        clickEvent: () => {}
      }
    }
  },

  mutations: {
    /**
     * 案例模板
     * @param state
     * @param data
     * @constructor
     */
    UPDATE_DEMO: (state, data) => {

    },
    /**
     * 更新 页面标题
     */
    UPDATE_HEADEROBJ(state, data) {
      const headerObj = data.headerObj
      // console.log(headerObj)
      if (validate.isNull(headerObj)) {
        // test todo
        if (process.env.ENV_CONFIG === 'sit') {
          // console.log('headerObj-顶部使用默认设置（测试ios手机无法显示头部的问题：）', headerObj)
        }

        return
      }

      state.headerObj = common.deepCloneObj(state.headerObjCopy)

      for (const k1 in state.headerObj) {
        // debugger
        if (headerObj.hasOwnProperty(k1)) {
          if (validate.judgeTypeOf(headerObj[k1]) === 'Object') {
            for (const k2 in headerObj[k1]) {
              state.headerObj[k1][k2] = headerObj[k1][k2]
            }
          } else {
            state.headerObj[k1] = headerObj[k1]
          }
        }
      }
      // test todo
      if (process.env.ENV_CONFIG === 'sit') {
        // console.log('headerObj-顶部使用自定义设置（测试ios手机无法显示头部的问题：）', headerObj)
      }
    }
  },
  actions: {
    /**
     * 案例模板
     * @param commit
     * @param data
     * @constructor
     */
    UpdateDemo({ commit }, data) {
      commit('UPDATE_DEMO', data)
    },
    /**
     * 更新 页面标题
     */
    UpdateHeaderObj(context, headerObj) {
      context.commit('UPDATE_HEADEROBJ', headerObj)
    }
  }
}

export default navigation

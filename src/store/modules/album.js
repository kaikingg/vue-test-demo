/**
 * 相册
 */
const album = {
  namespaced: true,
  state: {
    albumPublishData: {} // 发布相册（未保存的数据都将被存储）
  },
  mutations: {
    /**
     * 更新未保存的相册数据
     * @param {Object} state
     * @param {Number} data
     */
    UPDATE_ALBUMPUBLISHDATA(state, data) {
      state.albumPublishData = data
    }
  },
  actions: {
    /**
     * 更新未保存的相册数据
     * @param {Object} context
     * @param {Number} data
     */
    UpdateAlbumPublishData(context, data) {
      context.commit('UPDATE_ALBUMPUBLISHDATA', data)
    }
  }
}
export default album

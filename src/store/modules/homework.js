/**
 * 亲子任务
 */
const homework = {
  namespaced: true,
  state: {
    homeworkPublishData: {}, // 发布亲子任务（未保存的数据都将被存储）
    guardianSelectTab: 0 // 家长端亲子任务管理，tab索引
  },
  mutations: {
    /**
     * 更新未保存的亲子任务数据
     * @param {Object} state
     * @param {Number} data
     */
    UPDATE_HOMEWORKPUBLISHDATA(state, data) {
      state.homeworkPublishData = data
    },
    /**
     * 更新家长端亲子任务管理，tab索引
     * @param {Object} state
     * @param {Number} data
     */
    UPDATE_GUARDIANSELECTTAB(state, data) {
      state.guardianSelectTab = data
    }
  },
  actions: {
    /**
     * 更新未保存的亲子任务数据
     * @param {Object} context
     * @param {Number} data
     */
    UpdateHomeworkPublishData(context, data) {
      context.commit('UPDATE_HOMEWORKPUBLISHDATA', data)
    },
    /**
     * 更新未保存的亲子任务数据
     * @param {Object} context
     * @param {Number} data
     */
    UpdateGuardianSelectTab(context, data) {
      context.commit('UPDATE_GUARDIANSELECTTAB', data)
    }
  }
}
export default homework

/**
 * 在线报名
 */
import ext from '@/ext'
import store from '@/store'
const recruit = {
  namespaced: true,
  state: {
    recruitData: { // 在线报名数据
      id: 'init'
    }
  },
  mutations: {
    /**
     * 案例模板
     * @param state
     * @param data
     * @constructor
     */
    UPDATE_DEMO: (state, data) => {

    },
    /**
     * 在线报名数据
     */
    UPDATE_RECRUITDATA(state, recruitData) {
      state.recruitData = recruitData
    }
  },
  actions: {
    /**
     * 案例模板
     * @param commit
     * @param data
     * @constructor
     */
    UpdateDemo({ commit }, data) {
      commit('UPDATE_DEMO', data)
    },
    /**
     * 在线报名数据
     */
    UpdateRecruitData: {
      root: true,
      handler(context, recruitData) {
        context.commit('UPDATE_RECRUITDATA', recruitData)
      }
    },
    /**
     * 获取报名信息
     */
    GetRecruitData(context, params) {
      return new Promise((resolve, reject) => {
        const data = store.state.kg.recruitData
        if (data.id === 'init') { // init 为初始化数据
          ext.kg.getRecruitData(params).then(res => {
            resolve(res)
          }).catch(e => {
            reject(e)
          })
        } else {
          resolve(data)
        }
      })
    }
  }
}

export default recruit

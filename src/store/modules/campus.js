
const campus = {
  namespaced: true,
  state: {
    campusData: {
      bannerList: { isLoad: false, val: [] }, // banner轮播图
      latestNews: { isLoad: false, val: [] }, // 最新消息数组
      latestAlbum: { isLoad: false, val: [] }, // 最新相册数组
      latestGuardianHomework: { isLoad: false, val: [] }, // 最新作业（家长）
      latestStaffHomework: { isLoad: false, val: [] }, // 最新作业（教师）
      latestGuardianAttendance: { isLoad: false, val: [] }, // 今日考勤(家长)
      latestStaffAttendance: { isLoad: false, val: [] }, // 今日考勤（教师）
      latestLeaderAttendance: { isLoad: false, val: [] }, // 今日考勤（园长）
      latestRecipes: { isLoad: false, val: [] }, // 今日食谱
      latestAvoidFood: { isLoad: false, val: [] }, // 今日忌口
      curStaffClassSexRadio: { isLoad: false, val: [] }, // 本班男女比例
      classDistribution: { isLoad: false, val: [] }, // 班级分布（园长）
      classSexRadio: { isLoad: false, val: [] }, // 班级男女比例（管理员）
      gardentSexRatio: { isLoad: false, val: [] }, // 全园男女比例(管理员)
      gradentStatic: { isLoad: false, val: [] }, // 全园统计(管理员)
      studentDistribution: { isLoad: false, val: [] } // 学生分布类型(管理员)
    }
  },

  mutations: {
    /**
     * 案例模板
     * @param state
     * @param data
     * @constructor
     */
    UPDATE_DEMO: (state, data) => {

    },
    /**
     * 更新主页数据
     * @param state
     * @param data (格式： {key: '变量名'， val: '改变的值' , isUpdate: '是否需要刷新'})
     * @constructor
     */
    UPDATE_CAMPUS: (state, data) => {
      // console.log(data, '#######################')
      const selectData = state.campusData[data.key]
      if (data.isUpdate) {
        selectData.isLoad = false
      } else {
        selectData.val = data.val
        selectData.isLoad = true
      }
    },
    EMPTY_CAMPUS: (state) => {
      for (const item in state.campusData) {
        const _item = state.campusData[item]
        _item.isLoad = false
        _item.val = []
      }
    }
  },

  actions: {
    /**
     * 案例模板
     * @param commit
     * @param data
     * @constructor
     */
    UpdateDemo({ commit }, data) {
      commit('UPDATE_DEMO', data)
    },
    UpdateCampus(context, data) {
      context.commit('UPDATE_CAMPUS', data)
    },
    EmptyCampus(context) {
      context.commit('EMPTY_CAMPUS')
    }
  }
}

export default campus

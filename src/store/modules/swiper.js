/**
 * swiper
 */
const swiper = {
  namespaced: true,
  state: {
    swiperActiveIndex: 0 // swiper 图片轮播当前索引
  },

  mutations: {
    /**
     * 案例模板
     * @param state
     * @param data
     * @constructor
     */
    UPDATE_DEMO: (state, data) => {

    },
    UPDATE_SWIPERACTIVEINDEX(state, index) {
      state.swiperActiveIndex = index
    }
  },

  actions: {
    /**
     * 案例模板
     * @param commit
     * @param data
     * @constructor
     */
    UpdateDemo({ commit }, data) {
      commit('UPDATE_DEMO', data)
    },
    updateSwiperActiveIndex(context, index) { // swiper 图片轮播当前索引
      context.commit('UPDATE_SWIPERACTIVEINDEX', index)
    }
  }
}

export default swiper

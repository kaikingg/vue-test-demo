/**
 * 消息（通知，相册，作业，聊天）
 */

const news = {
  namespaced: true,
  state: {
    dynamicInfos: 0, // 动态信息数量状态
    mailInfos: 0, // 通信录信息数量状态
    chatInfoNum: 0, // websocket 信息 发送 状态 chatInfoNum
    chatInfoReceive: 0, // websocket 信息 接收 状态
    websocketInfo: null
  },

  mutations: {
    /**
     * 案例模板
     * @param state
     * @param data
     * @constructor
     */
    UPDATE_DEMO: (state, data) => {

    },
    UPDATE_DYNAMICINFOS(state, dynamicInfos) { // 动态信息数量状态
      state.dynamicInfos = dynamicInfos
    },
    UPDATE_MAILINFOS(state, mailInfos) { // 通信录信息数量状态
      state.mailInfos = mailInfos
    },
    UPDATE_CHATINFONUM(state, chatInfoNum) { // 信息 发送 状态
      state.chatInfoNum = chatInfoNum
    },
    UPDATE_CHATINFORECEIVE(state, chatInfoReceive) { // 信息 接收 状态
      state.chatInfoReceive = chatInfoReceive
    },
    UPDATE_WEBSOCKETINFO(state, data) {
      state.websocketInfo = data
    }
  },

  actions: {
    /**
     * 案例模板
     * @param commit
     * @param data
     * @constructor
     */
    UpdateDemo({ commit }, data) {
      commit('UPDATE_DEMO', data)
    },
    updateDynamicInfos(context, dynamicInfos) { // 动态信息数量状态
      context.commit('UPDATE_DYNAMICINFOS', dynamicInfos)
    },
    updateMailInfos(context, mailInfos) { // 通信录信息数量状态
      context.commit('UPDATE_MAILINFOS', mailInfos)
    },
    updateChatInfoNum(context, chatInfoNum) { // 信息 发送 状态
      context.commit('UPDATE_CHATINFONUM', chatInfoNum)
    },
    updateChatInfoReceive(context, chatInfoReceive) { // 信息 接收 状态
      context.commit('UPDATE_CHATINFORECEIVE', chatInfoReceive)
    }
  }
}

export default news

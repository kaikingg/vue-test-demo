/**
 * upload 图片上传组件
 */
const upload = {
  namespaced: true,
  state: {
    isUploadImage: false, // 是否上传图片
    uploadImageTxt: '正在上传图片',
    picNewArr: [], // 新增图数组
    picOriginArr: [], // 原图数组

    isUploadVideo: false, // 是否上传视频
    uploadVideoTxt: '正在上传视频',
    videoNewArr: [], // 新增视频数组
    videoOriginArr: [] // 原视频数组
  },

  mutations: {
    /**
     * 是否上传图片
     * @param {Object} state
     * @param {Array} isUploadImage
     */
    UPDATE_ISUPLOADIMAGE(state, isUploadImage) {
      state.isUploadImage = Boolean(isUploadImage)
    },
    /**
     * 上传图片提示语
     * @param {Object} state
     * @param {Array} uploadImageTxt
     */
    UPDATE_UPLOADIMAGETXT(state, uploadImageTxt) {
      state.uploadImageTxt = uploadImageTxt
    },
    /**
     * 新增图+原始图
     * @param {Object} state
     * @param {Array} allPicArr
     */
    // UPDATE_ALLPICARR(state, data) {
    //   state.allPicArr = data
    // },
    /**
     * 新增图数组
     * @param {Object} state
     * @param {Array} picNewArr
     */
    UPDATE_PICNEWARR(state, picNewArr) {
      state.picNewArr = picNewArr
    },
    /**
     * 原图数组
     * @param {Object} state
     * @param {Array} picOriginArr
     */
    UPDATE_PICORIGINARR(state, picOriginArr) {
      state.picOriginArr = picOriginArr
    },

    /**
     * 是否上传视频
     * @param {Object} state
     * @param {Array} isUploadVideo
     */
    UPDATE_ISUPLOADVIDEO(state, data) {
      state.isUploadVideo = Boolean(data)
    },
    /**
     * 上传视频提示语
     * @param {Object} state
     * @param {Array} uploadVideoTxt
     */
    UPDATE_UPLOADVIDEOTXT(state, data) {
      state.uploadVideoTxt = data
    },
    /**
     * 新增视频数组
     * @param {Object} state
     * @param {Array} videoNewArr
     */
    UPDATE_VIDEONEWARR(state, data) {
      state.videoNewArr = data
    },
    /**
     * 原视频数组
     * @param {Object} state
     * @param {Array} videoOriginArr
     */
    UPDATE_VIDEOORIGINARR(state, data) {
      state.videoOriginArr = data
    }
  },

  actions: {
    /**
     * 是否上传图片
     * @param {Object} context
     * @param {Array} isUploadImage
     */
    UpdateIsUploadImage(context, isUploadImage) {
      context.commit('UPDATE_ISUPLOADIMAGE', isUploadImage)
    },
    /**
     * 上传图片提示语
     * @param {Object} context
     * @param {Array} uploadImageTxt
     */
    UpdateIsUploadImageTxt(context, uploadImageTxt) {
      context.commit('UPDATE_UPLOADIMAGETXT', uploadImageTxt)
    },
    /**
     * 新增图数组
     * @param {Object} context
     * @param {Array} picNewArr
     */
    UpdatePicNewArr(context, picNewArr) {
      context.commit('UPDATE_PICNEWARR', picNewArr)
    },
    /**
     * 原图数组
     * @param {Object} context
     * @param {Array} picOriginArr
     */
    UpdatePicOriginArr(context, picOriginArr) {
      context.commit('UPDATE_PICORIGINARR', picOriginArr)
    },

    /**
     * 是否上传视频

     * @param {Object} context
     * @param {Array} isUploadVideo
     */
    UpdateIsUploadVideo(context, data) {
      context.commit('UPDATE_ISUPLOADVIDEO', data)
    },
    /**
     * 上传视频提示语
     * @param {Object} context
     * @param {Array} uploadVideoTxt
     */
    UpdateIsUploadVideoTxt(context, data) {
      context.commit('UPDATE_UPLOADVIDEOTXT', data)
    },
    /**
     * 新增视频数组
     * @param {Object} context
     * @param {Array} videoNewArr
     */
    UpdateVideoNewArr(context, data) {
      context.commit('UPDATE_VIDEONEWARR', data)
    },
    /**
     * 原视频数组
     * @param {Object} context
     * @param {Array} videoOriginArr
     */
    UpdateVideoOriginArr(context, data) {
      context.commit('UPDATE_VIDEOORIGINARR', data)
    }
  }
}

export default upload

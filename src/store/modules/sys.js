/**
 * 系统全局
 */
import validate from '@/utils/open/validate'
import ext from '@/ext'
import store from '../index'
// import api from '@/api'
// const shouldUseTransition = !/transition=none/.test(location.href)
const sys = {
  namespaced: true,
  state: {
    historyCount: 0,
    // bannerList: [], // 首页banner
    routerNameObj: {
      Home: 0
    },
    prevRouterName: 'Home', // 上一次路由
    prevRouterObj: {}, // 上一次路由
    // direction: shouldUseTransition ? 'forward' : '', // 页面切换专场效果记录
    direction: '', // 页面切换专场效果记录
    timerChangeToken: null, // 定时切换token
    isWechat: false, // 是否是微信坏境
    dictionaryData: {}, // 数据字典数据
    scrollGetPosTimer: null, // 获取scroller定位的定时器id
    isLoading: true, // 正在加载中
    isTransition: false, // 是否转场，页面切换启动
    isShareMaskShow: false, // 是否打开分享引导页,
    rollbackStatus: false // 记录回滚路由状态
  },
  mutations: {
    /**
     * 案例模板
     * @param state
     * @param data
     * @constructor
     */
    UPDATE_DEMO: (state, data) => {},
    /**
     * 记录路由跳转数量
     */
    UPDATE_HISTORYCOUNT(state, historyCount) {
      state.historyCount = historyCount
    },
    /**
     * 上一次路由名字
     */
    UPDATE_PREVROUTEROBJ(state, prevRouterObj) {
      state.prevRouterObj = prevRouterObj
    },
    /**
     * 上一次路由名字
     */
    UPDATE_PREVROUTERNAME(state, prevRouterName) {
      state.prevRouterName = prevRouterName
    },
    /**
     * 上一路由链接 对象
     */
    UPDATE_ROUTERNAMEOBJ(state, routerNameObj) {
      state.routerNameObj = routerNameObj
    },
    /**
     * 页面切换专场效果记录
     */
    UPDATE_DIRECTION(state, direction) {
      state.direction = direction
    },
    /**
     * 定时刷新token 定时器id
     */
    UPDATE_TIMERCHANGETOKEN(state, timerChangeToken) {
      if (validate.isNull(timerChangeToken)) { // 传递为空则表示清除定时器
        clearInterval(this.state.sys.timerChangeToken)
      }
      state.timerChangeToken = timerChangeToken
    },
    /**
     * 更新是否是微信坏境
     */
    UPDATE_ISWECHAT(state, isWechat) {
      state.isWechat = isWechat
    },
    /**
     * 更新数据字典数据
     */
    UPDATE_DICTIONARYDATA(state, dictionaryData) {
      state.dictionaryData = dictionaryData
    },
    /**
     * 更新scroller定位的定时器id
     */
    UPDATE_SCROLLGETPOSTIMER(state, scrollGetPosTimer) {
      state.scrollGetPosTimer = scrollGetPosTimer
    },
    /**
     * 更新是否加载中
     */
    UPDATE_ISLOADING(state, isLoading) {
      state.isLoading = isLoading
    },
    /**
     * 是否转场，页面切换启动 isTransition
     */
    UPDATE_ISTRANSITION(state, data) {
      state.isTransition = data
    },
    /**
     * 是否显示分享页
     */
    UPDATE_ISSHAREMASKSHOW(state, data) {
      state.isShareMaskShow = data
    },
    /**
     * 记录回滚路由状态，进去为false，退出为ture
     */
    UPDATE_ROLLBACK_STATUS(state, data) {
      state.rollbackStatus = data
    }
  },
  actions: {
    /**
     * 案例模板
     * @param commit
     * @param data
     * @constructor
     */
    UpdateDemo({ commit }, data) {
      commit('UPDATE_DEMO', data)
    },
    /**
     * 记录回滚路由状态，进去为false，退出为ture
     */
    UpdateRollbackStatus(context, status) {
      context.commit('UPDATE_ROLLBACK_STATUS', status)
    },
    /**
     * 记录路由跳转数量
     */
    UpdateHistoryCount(context, historyCount) {
      context.commit('UPDATE_HISTORYCOUNT', historyCount)
    },
    /**
     * 上一次路由名字
     */
    UpdatePrevRouterName(context, prevRouterName) {
      context.commit('UPDATE_PREVROUTERNAME', prevRouterName)
    },
    /**
     * 上一路由链接 对象
     */
    UpdatePrevRouterObj(context, prevRouterObj) {
      context.commit('UPDATE_PREVROUTEROBJ', prevRouterObj)
    },
    /**
     * 记录路由跳转 对象
     */
    UpdateRouterNameObj(context, routerNameObj) {
      context.commit('UPDATE_ROUTERNAMEOBJ', routerNameObj)
    },
    /**
     * 页面切换专场效果记录
     */
    UpdateDirection(context, direction) {
      context.commit('UPDATE_DIRECTION', direction)
    },
    /**
     * 更新是否是微信坏境
     */
    UpdateTimerChangeToken(context, timerChangeToken) {
      context.commit('UPDATE_TIMERCHANGETOKEN', timerChangeToken)
    },
    /**
     * 更新是否是微信坏境
     */
    UpdateIsWechat(context, isWechat) {
      context.commit('UPDATE_ISWECHAT', isWechat)
    },
    /**
     * 更新数据字典数据
     */
    UpdateDictionaryData: {
      root: true,
      handler(context, dictionaryData) {
        context.commit('UPDATE_DICTIONARYDATA', dictionaryData)
      }
    },
    /**
     * 更新scroller定位的定时器id
     */
    UpdateScrollGetPosTimer(context, scrollGetPosTimer) {
      context.commit('UPDATE_SCROLLGETPOSTIMER', scrollGetPosTimer)
    },
    /**
     * 更新是否加载中
     */
    UpdateIsLoading(context, isLoading) {
      context.commit('UPDATE_ISLOADING', isLoading)
    },
    /**
     * 是否转场，页面切换启动 isTransition
     */
    UpdateIsTransition(context, flag) {
      // debugger
      context.commit('UPDATE_ISTRANSITION', flag)
    },
    UpdateIsShareMaskShow(context, data) {
      context.commit('UPDATE_ISSHAREMASKSHOW', data)
    },
    /**
     * 获取数据字典
     * @params {Object} params { dictType: type, isOrderBy: isOrderBy }
     */
    GetDicData(context, params) {
      return new Promise((resolve, reject) => {
        const dicData = this.state.sys.dictionaryData[params.dictType]
        // debugger
        if (dicData) {
          resolve(dicData)
        } else {
          // { dictType:'', type:'', isOrderBy:'' }
          ext.sys.getDicData(params.dictType, params.type, params.isOrderBy).then(res => {
            resolve(res)
          }).catch(e => {
            reject(e)
          })
        }
      })
    },
    /**
     * 获取首页banner图
     */
    GetBannerList(context, data) {
      return new Promise((resolve, reject) => {
        ext.sys.getBannerList().then(res => {
          resolve(res)
          // context.commit('UPDATE_BANNERLIST', res.result)
          store.dispatch('campus/UpdateCampus', { key: 'bannerList', val: res.result })
        }).catch(e => {
          reject(e)
        })
        // const bannerData = this.state.sys.bannerList || []
        // if (bannerData.length) {
        //   // debugger
        //   context.commit('UPDATE_BANNERLIST', [])
        //   context.commit('UPDATE_BANNERLIST', bannerData)
        //   resolve(bannerData)
        // } else {
        //   // debugger
        //   ext.sys.getBannerList().then(res => {
        //     resolve(res)
        //     context.commit('UPDATE_BANNERLIST', res.result)
        //   }).catch(e => {
        //     reject(e)
        //   })
        // }
      })
    }
  }
}

export default sys

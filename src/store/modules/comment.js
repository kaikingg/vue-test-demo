
const campus = {
  namespaced: true,
  state: {
    commentTxt: '',
    isShow: false,
    maskShow: false,
    maskSearchShow: false,
    isCommentRefresh: false,
    pid: '',
    params: {
      pid: '',
      mutualCommentId: '',
      mutualCommentatorId: ''
    },
    isSearchFocus: false,
    detail: {}
  },

  mutations: {
    /**
     * 案例模板
     * @param state
     * @param data
     * @constructor
     */
    UPDATE_DEMO: (state, data) => {

    },
    /**
     * 更新主页数据
     * @param state
     * @param data
     * @constructor
     */
    UPDATE_COMMENTTXT: (state, data) => {
      state.commentTxt = data
    },
    UPDATE_ISSHOW: (state, data) => {
      state.isShow = data
    },
    UPDATE_MASKSHOW: (state, data) => {
      state.maskShow = data
    },
    UPDATE_PARAMS: (state, data) => {
      state.params = data
    },
    UPDATE_PID: (state, data) => {
      state.pid = data
    },
    UPDATE_DETAIL: (state, data) => {
      state.detail = data
    },
    UPDATE_COMMENTREFRESH: (state, data) => {
      state.isCommentRefresh = data
    },
    UPDATE_ISSEARCHFOCUS: (state, data) => {
      state.isSearchFocus = data
    },
    UPDATE_MASKSEARCHSHOW: (state, data) => {
      state.maskSearchShow = data
    },
    RESET_PARAMS: (state, data) => {
      for (const x in state.params) {
        state.params[x] = ''
      }
    }
  },

  actions: {
    /**
     * 案例模板
     * @param commit
     * @param data
     * @constructor
     */
    UpdateCommentTxt({ commit }, data) {
      commit('UPDATE_COMMENTTXT', data)
    },
    UpdateShow: ({ commit }, data) => {
      commit('UPDATE_ISSHOW', data)
    },
    UpdateMaskShow: ({ commit }, data) => {
      commit('UPDATE_MASKSHOW', data)
    },
    UpdateParams: ({ commit }, data) => {
      commit('UPDATE_PARAMS', data)
    },
    UpdatePid: ({ commit }, data) => {
      commit('UPDATE_PID', data)
    },
    UpdateDetail: ({ commit }, data) => {
      commit('UPDATE_DETAIL', data)
    },
    UpdateCommentRefresh: ({ commit }, data) => {
      commit('UPDATE_COMMENTREFRESH', data)
    },
    UpdateSearchFocus: ({ commit }, data) => {
      commit('UPDATE_ISSEARCHFOCUS', data)
    },
    UpdateMskSearchShow: ({ commit }, data) => {
      commit('UPDATE_MASKSEARCHSHOW', data)
    },
    ResetParams: ({ commit }, data) => {
      commit('RESET_PARAMS', data)
    }
  }
}

export default campus

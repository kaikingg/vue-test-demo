/**
 * 该状态 为页面状态控制
 */

const vuxTool = {
  namespaced: true,
  state: {
    isShowDrawer: false,
    isBindWechatDialog: false, // 绑定微信弹窗
    isDefaulutUnitDialog: false // 设置默认单位弹窗
  },
  mutations: {
    /**
     * 案例模板
     * @param state
     * @param data
     * @constructor
     */
    UPDATE_DEMO: (state, data) => {

    },
    /**
     * 更新侧边栏 drawer 显示状态
     */
    UPDATE_ISSHOWDRAWER(state, isShowDrawer) {
      state.isShowDrawer = isShowDrawer
    },
    /**
     * 更新 isDefaulutUnitDialog 设置默认弹窗
     */
    UPDATE_ISDEFAULTUNITDIALOG(state, isDefaulutUnitDialog) {
      // debugger
      state.isDefaulutUnitDialog = isDefaulutUnitDialog
    },
    /**
     * 更新 isBindWechatDialog 绑定微信弹窗
     */
    UPDATE_ISBINDWECHATDIALOG(state, isBindWechatDialog) {
      // debugger
      state.isBindWechatDialog = isBindWechatDialog
    }
  },
  actions: {
    /**
     * 案例模板
     * @param commit
     * @param data
     * @constructor
     */
    UpdateDemo({ commit }, data) {
      commit('UPDATE_DEMO', data)
    },
    /**
     * 更新侧边栏 drawer 显示状态
     */
    updateIsShowDrawer: {
      root: true,
      handler(context, isShowDrawer) {
        context.commit('UPDATE_ISSHOWDRAWER', isShowDrawer)
      }
    },
    /**
     * 更新 isDefaulutUnitDialog 设置默认弹窗
     */
    updateIsDefaulutUnitDialog: {
      root: true,
      handler(context, isDefaulutUnitDialog) {
        // debugger
        context.commit('UPDATE_ISDEFAULTUNITDIALOG', isDefaulutUnitDialog)
      }
    },
    /**
     * 更新 isBindWechatDialog 绑定微信弹窗
     */
    updateIsBindWechatDialog: {
      root: true,
      handler(context, isBindWechatDialog) {
        context.commit('UPDATE_ISBINDWECHATDIALOG', isBindWechatDialog)
      }
    }
  }
}

export default vuxTool

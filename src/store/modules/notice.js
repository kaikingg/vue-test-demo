/**
 * 通知
 */
const notice = {
  namespaced: true,
  state: {
    selectTabObj: {
      routeName: '', // 当前选中tab 的索引和tab页路由名字
      index: 0 // tab名字
    },
    noticePublishData: {} // 发布通知（未保存的数据都将被存储）
  },
  mutations: {
    /**
     * 更新选中索引 selectTabObj
     * @param {Object} state
     * @param {Number} selectTabObj
     */
    UPDATE_SELECTTABOBJ(state, selectTabObj) {
      state.selectTabObj = selectTabObj
    },
    /**
     * 更新未保存的通知数据
     * @param {Object} state
     * @param {Number} data
     */
    UPDATE_NOTICEPUBLISHDATA(state, data) {
      state.noticePublishData = data
    }
  },
  actions: {
    /**
     * 更新选中索引 selectTabObj
     * @param {Object} context
     * @param {Number} selectTabObj
     */
    UpdateSelectTabObj(context, selectTabObj) {
      context.commit('UPDATE_SELECTTABOBJ', selectTabObj)
    },
    /**
     * 更新未保存的通知数据
     * @param {Object} context
     * @param {Number} data
     */
    UpdateNoticePublishData(context, data) {
      context.commit('UPDATE_NOTICEPUBLISHDATA', data)
    }
  }
}
export default notice

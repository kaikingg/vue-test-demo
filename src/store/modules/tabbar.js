/**
 * 底部导航选项卡状态管理
 * @type {{namespaced: boolean, state: {selectTabIndex: number, isShow: boolean}, mutations: {UPDATE_SELECT_TAB_INDEX(*, *): void}, actions: {UpdateHeaderObj(*, *=): void}}}
 */
const tabbar = {
  namespaced: true,
  state: {
    selectTabIndex: 0,
    isShowTabber: true
  },

  mutations: {
    /**
     * 案例模板
     * @param state
     * @param data
     * @constructor
     */
    UPDATE_DEMO: (state, data) => {

    },
    /**
     * 更新选中底部导航的序号 selectTabIndex
     */
    UPDATE_SELECT_TAB_INDEX(state, data) {
      state.selectTabIndex = data
    },
    /**
     * 更新选中底部导航的是否显示隐藏
     */
    UPDATE_IS_SHOW(state, data) {
      state.isShowTabber = data
    }
  },
  actions: {
    /**
     * 案例模板
     * @param commit
     * @param data
     * @constructor
     */
    UpdateDemo({ commit }, data) {
      commit('UPDATE_DEMO', data)
    },
    /**
     * 更新选中底部导航的序号 selectTabIndex
     */
    UpdateSelectTabIndex(context, data) {
      context.commit('UPDATE_SELECT_TAB_INDEX', data)
    },
    /**
     * 更新选中底部导航的是否显示隐藏
     */
    UpdateIsShowTabber(context, data) {
      context.commit('UPDATE_IS_SHOW', data)
    }
  }
}

export default tabbar

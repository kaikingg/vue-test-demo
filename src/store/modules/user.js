import api from '@/api'
import store from '../index'
import ext from '@/ext'

const user = {
  namespaced: true,
  state: {
    isCutOutUrl: false, // isCutOutUrl 是否已截取链接，每次进来，在关闭浏览器之前，只允许截取一次
    isLogin: false, // 是否已登录
    token: '',
    refreshToken: '', // 定时刷新token
    weChatInfo: {}, // 当前用户微信数据
    curSelectUnit: {}, // 当前选中的单位
    curSelectUnitId: {}, // 当前选中的单位id
    curSelectRole: {
      sexObj: {}
    }, // 当前选中的角色
    curSelectRoleId: {}, // 当前选中的角色id
    unitList: [], // 单位数据
    keepAliveList: [], // 已经缓存的路由
    excludeList: [], // 不需要缓存的路由
    unreadNum: '',
    firstUrlParam: {}, // 首页链接传参
    userInfo: [],
    permission: {}, // 权限code
    unreadList: {},
    imgList: [], // 已上传图片数组
    codeUserInfo: {}, // 中央用户信息
    tempUnitId: '' // 游客用户临时单位ID
  },

  mutations: {
    /**
     * 案例模板
     * @param state
     * @param data
     * @constructor
     */
    UPDATE_DEMO: (state, data) => {

    },
    /**
     * firstUrlParam 值更新
     * @param {*} state
     * @param {*} firstUrlParam
     */
    UPDATE_FIRSTURLPARAM(state, firstUrlParam) {
      state.firstUrlParam = firstUrlParam
    },
    /**
     * isLogin 是否已登录
     * @param {*} state
     * @param {*} token
     */
    UPDATE_ISLOGIN(state, isLogin) {
      state.isLogin = isLogin
    },
    /**
     * refreshToken 定时刷新token
     * @param {*} state
     * @param {*} refreshToken
     */
    UPDATE_REFRESHTOKEN(state, refreshToken) {
      state.refreshToken = refreshToken
    },
    /**
     * isCutOutUrl 是否已截取链接，每次进来，在关闭浏览器之前，只允许截取一次
     * @param {*} state
     * @param {*} isCutOutUrl
     */
    UPDATE_ISCUTOUTUTL(state, isCutOutUrl) {
      state.isCutOutUrl = isCutOutUrl
    },
    /**
     * token 值更新
     * @param {*} state
     * @param {*} token
     */
    UPDATE_TOKEN(state, token) {
      state.token = token
    },
    /**
     * weChatInfo 值更新
     * @param {*} state
     * @param {*} weChatInfo
     */
    UPDATE_WECHATINFO(state, weChatInfo) {
      state.weChatInfo = weChatInfo
    },
    /**
     * curSelectUnitId 值更新
     * @param {*} state
     * @param {*} curSelectUnitId
     */
    UPDATE_CURSELECTUNITID(state, curSelectUnitId) {
      state.curSelectUnitId = curSelectUnitId
    },
    /**
     * 当前选中角色 值更新
     * @param {*} state
     * @param {*} curSelectRole
     */
    UPDATE_CURSELECTROLE(state, curSelectRole) {
      state.curSelectRole = curSelectRole
    },
    /**
     * 当前选中角色id 值更新
     * @param {*} state
     * @param {*} curSelectRoleId
     */
    UPDATE_CURSELECTROLEID(state, curSelectRoleId) {
      state.curSelectRoleId = curSelectRoleId
    },
    /**
     * curSelectUnit 值更新
     * @param {*} state
     * @param {*} curSelectUnit
     */
    UPDATE_CURSELECTUNIT(state, curSelectUnit) {
      state.curSelectUnit = curSelectUnit
    },
    /**
     * unitList 值更新
     * @param {*} state
     * @param {*} unitList
     */
    UPDATE_UNITLIST(state, unitList) {
      state.unitList = unitList || []
    },
    /**
     * unreadNum 值更新
     * @param {*} state
     * @param {*} num
     */
    UPDATE_UNREADNUM(state, num) {
      state.unreadNum = num
    },
    /**
     * 用户信息 值更新
     * @param {*} state
     * @param {*} userInfo
     */
    UPDATE_USERINFO(state, userInfo) {
      state.userInfo = userInfo
    },
    /**
     * 更新 未读数据列表
     * @param {*} state
     * @param {*} obj
     */
    UPDATE_UNREADLIST(state, obj) {
      state.unreadList[obj.key] = obj.val
    },
    REDUCE_UNREADLIST(state, key) {
      state.unreadList[key] -= 1
    },
    /**
     * 更新 不需要保存的路由name
     * @param {*} state
     * @param {*} data
     */
    UPDATE_EXCLUDELIST(state, data) {
      state.excludeList = data
    },
    /**
     * 更新 已保存的路由name
     * @param {*} state
     * @param {*} data
     */
    UPDATE_KEEPALIVELIST(state, data) {
      state.keepAliveList = data
    },
    /**
     * 更新 临时单位id
     * @param {*} state
     * @param {*} data
     */
    UPDATE_TENPUNITID(state, data) {
      state.tempUnitId = data
    },
    /**
     * 资源权限数据对象，用于页面显示隐藏资源菜单
     * @param state
     * @param permissions
     * @constructor
     */
    SET_PERMISSIONS: (state, permissions) => {
      const list = {}
      for (let i = 0; i < permissions.length; i++) {
        list[permissions[i]] = true
      }
      state.permission = list
    },
    /**
     * 清除权限
     * @param state
     */
    CLEAR_PERMISSIONS: (state) => {
      const list = state.permission
      const result = {}
      // for (const i in list) {
      //   result[i] = false
      // }
      state.permission = result
    },
    /**
     * 更新 上传图片数组
     * @param state
     * @param arr
     */
    SET_UPLOADIMAGE: (state, imgList) => {
      state.imgList = imgList
    },
    /**
     *  修改中央用户信息
     */
    SET_CODECUSERINFO: (state, data) => {
      state.codeUserInfo = data
    }
  },

  actions: {
    /**
     * 案例模板
     * @param commit
     * @param data
     * @constructor
     */
    UpdateDemo({ commit }, data) {
      commit('UPDATE_DEMO', data)
    },
    UpdateFirstUrlParam(context, firstUrlParam) {
      context.commit('UPDATE_FIRSTURLPARAM', firstUrlParam)
    },
    UpdateIsLogin(context, isLogin) {
      context.commit('UPDATE_ISLOGIN', isLogin)
    },
    UpdateRefreshToken(context, refreshToken) {
      context.commit('UPDATE_REFRESHTOKEN', refreshToken)
    },
    UpdateIsCutOutUrl(context, isCutOutUrl) {
      context.commit('UPDATE_ISCUTOUTUTL', isCutOutUrl)
    },
    UpdateCurSelectUnit(context, curSelectUnit) {
      context.commit('UPDATE_CURSELECTUNIT', curSelectUnit)
    },
    UpdateCurSelectUnitId(context, curSelectUnitId) {
      context.commit('UPDATE_CURSELECTUNITID', curSelectUnitId)
    },
    UpdateWeChatInfo(context, weChatInfo) {
      context.commit('UPDATE_WECHATINFO', weChatInfo)
    },
    UpdateUploadedImage(context, imgList) {
      context.commit('SET_UPLOADIMAGE', imgList)
    },
    /**
     * 清除微信信息
     * @param {Object} context
     */
    CleanWeChatInfo(context) {
      context.commit('UPDATE_WECHATINFO', {})
      window.sessionStorage.removeItem('weChatInfo')
    },
    UpdateCurSelectRole(context, curSelectRole) {
      context.commit('UPDATE_CURSELECTROLE', curSelectRole)
    },
    UpdateCurSelectRoleId(context, curSelectRoleId) {
      context.commit('UPDATE_CURSELECTROLEID', curSelectRoleId)
    },
    UpdateToken(context, token) {
      context.commit('UPDATE_TOKEN', token)
    },
    UpdateUnitList(context, unitList) {
      context.commit('UPDATE_UNITLIST', unitList)
    },
    UpdateUserInfo(context, userinfo) {
      context.commit('UPDATE_USERINFO', userinfo)
    },
    UpdateUnreadNum(context, data) {
      context.commit('UPDATE_UNREADNUM', data)
    },
    /**
     *  更新缓存列表
     */
    UpdateExculdeList(context, data) {
      context.commit('UPDATE_EXCLUDELIST', data)
    },
    UpdateKeepAliveList(context, data) {
      context.commit('UPDATE_KEEPALIVELIST', data)
    },
    UpdateTempUnitId(context, data) {
      context.commit('UPDATE_TENPUNITID', data)
    },
    /**
     * g更新未读信息
     * @param type
     */
    UpdateUnreadList({ commit }, type) {
      // console.log(type)
      if (type === 0) {
        return new Promise((resolve, reject) => {
          api.user.getUnreadPlateNum().then((res) => {
            const tempObj = { key: 'plateNum', val: res.result - 0 }
            store.dispatch('user/UpdateUnreadNum', res.result - 0)
            store.commit('user/UPDATE_UNREADLIST', tempObj)
            resolve()
          }).catch(e => {
            reject(e)
          })
        })
      } else {
        api.user.getOhtersUnreadNum().then((res) => {
          // debugger
          let totalNum = store.state.user.unreadNum
          for (const i of res) {
            const arr = i.split(':')
            const tempObj = { key: arr[0], val: arr[1] - 0 }
            totalNum = totalNum + (arr[1] - 0)
            store.commit('user/UPDATE_UNREADLIST', tempObj)
          }
          store.commit('user/UPDATE_UNREADNUM', totalNum)
        }).catch(e => {})
      }
    },
    /**
     * 设置资源全部为false
     * @param context
     * @param data
     * @constructor
     */
    ClearPermissionsAll({ commit, state }) {
      commit('CLEAR_PERMISSIONS')
      commit('UPDATE_CURSELECTROLE', {})
    },
    ClearPermissions({ commit, state }) {
      commit('CLEAR_PERMISSIONS')
    },
    /**
     * 获取临时token
     * @param commit
     * @param data
     * @returns {Promise<any>}
     * @constructor
     */
    GetToken({ commit }, data) {
      return new Promise((resolve, reject) => {
        // ext.user.getToken
        ext.user.getToken().then((res) => {
          resolve(res)
        }).catch(e => {
          reject(e)
        })
      })
    },
    /**
     * 退出清除相关数据
     * @param commit
     * @param that
     * @returns {Promise<any>}
     * @constructor
     */
    LoginOut({ commit, state }) {
      window.localStorage.removeItem('userAccount')
      commit('UPDATE_ISLOGIN', false) // isLogin 登录状态
      commit('UPDATE_REFRESHTOKEN', null) // refreshToken 清空
      commit('UPDATE_TOKEN', null) // token 清空
      commit('UPDATE_UNITLIST', {}) // unitList 单位数据
      commit('UPDATE_CURSELECTUNIT', {}) // curSelectUnit 当前选中单位
      commit('UPDATE_CURSELECTUNITID', null) // curSelectUnitId 当前选中单位id
      commit('UPDATE_USERINFO', {}) // userInfo 用户档案数据
      commit('UPDATE_CURSELECTROLE', {}) // curSelectRole 当前选中角色
      commit('UPDATE_CURSELECTROLEID', null) // curSelectRoleId 当前选中角色id
    },
    /**
     * 登录校验
     * @param commit
     * @param that
     * @returns {Promise<any>}
     * @constructor
     */
    Login({ commit }, userAccount) {
      userAccount = userAccount || {}
      return new Promise((resolve, reject) => {
        // debugger
        ext.user.login(userAccount.name, userAccount.pwd, userAccount.loginType).then(res => {
          // debugger
          resolve(res)
        }).catch(e => {
          reject(e)
        })
      })
    },

    /**
     * 获取用户信息
     * @param commit
     * @param state
     * @returns {Promise<any>}
     * @constructor
     */
    GetUserInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        ext.user.getUserInfo().then(res => {
          resolve(res)
        }).catch(e => {
          reject(e)
        })
      })
    },
    /**
     * 获取权限菜单信息
     * @param commit
     * @param state
     * @returns {Promise<any>}
     * @constructor
     */
    GetPermission({ commit, state }, data) {
      if (data) {
        return new Promise((resolve, reject) => {
          api.user.getPermission(data).then(res => {
            commit('SET_PERMISSIONS', res)
            resolve(res)
          })
        })
      } else {
        store.commit('user/CLEAR_PERMISSIONS')
      }
    },
    /**
     * 获取单位数据信息
     * @param commit
     * @param data
     * @returns {Promise<any>}
     * @constructor
     */
    GetUnitList({ commit }, data) {
      return new Promise((resolve, reject) => {
        ext.user.getUnitList().then((res) => {
          resolve(res)
        }).catch(e => {
          reject(e)
        })
      })
    },
    ChangeUnit({ commit }, id) {
      return new Promise((resolve, reject) => {
        ext.user.changeUnit(id).then((res) => {
          resolve(res)
        }).catch(e => {
          reject(e)
        })
      })
    },
    /**
     * 获取微信数据信息
     * @param commit
     * @param data
     * @returns {Promise<any>}
     * @constructor
     */
    GetWechatInfo({ commit }, data) {
      return new Promise((resolve, reject) => {
        ext.wechat.getWechatInfo(data).then((res) => {
          resolve(res)
        }).catch(e => {
          reject(e)
        })
      })
    },
    /**
     *  请求中央用户信息
     */
    getCodeUserInfo({ commit }) {
      ext.user.getCodeUserInfo().then((res) => {
        store.commit('user/SET_CODECUSERINFO', res)
      })
    }
  }
}

export default user


const campus = {
  namespaced: true,
  state: {
    fileList: [],
    paramsList: {}
  },

  mutations: {
    /**
     * 案例模板
     * @param state
     * @param data
     * @constructor
     */
    UPDATE_DEMO: (state, data) => {

    },
    /**
     * 更新主页数据
     * @param state
     * @param data
     * @constructor
     */
    UPDATE_FILELIST: (state, data) => {
      state.fileList = data
    },
    /**
     * 更新主页数据
     * @param state
     * @param data
     * @constructor
     */
    UPDATE_PARAMSLIST: (state, data) => {
      state.paramsList = data
    }
  },

  actions: {
    /**
     * 案例模板
     * @param commit
     * @param data
     * @constructor
     */
    UpdateDemo({ commit }, data) {
      commit('UPDATE_DEMO', data)
    },
    UpdateFiles(context, data) {
      context.commit('UPDATE_FILELIST', data)
    },
    UpdateParams(context, data) {
      context.commit('UPDATE_PARAMSLIST', data)
    }
  }
}

export default campus

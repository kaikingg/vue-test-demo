import request from '@/utils/open/request'
import common from '@/utils/open/common'
// import store from '@/store'
import constant from '@/constant'
// import timePlugin from '@/utils/open/time-plugin'

/**
 * 幼教业务放置接口
 * 注意：API接口只做请求数据和格式化数据的事情，其他事务一概不处理
 */
export default {
  /**
   * 接口例子模板
   *
   * @param param
   * @param resolve
   * @param reject
   */
  demo(param, resolve, reject) {
    const url = common.static.ctx + '///'
    request.get(url, param).then(res => {
      if (res) {
        if (res.code === 0) {
          resolve(res)
        } else {
          reject(res)
        }
      } else {
        reject(res)
      }
    }).catch(error => {
      reject(error)
    })
  },
  /**
   * 获取作业详情
   * @param param
   * @param url
   */
  getHomeworkDetail(param, url) {
    const url1 = common.static.ctx + url
    return new Promise((resolve, reject) => {
      request.get(url1, param).then(res => {
        if (res) {
          if (res.code === 0) {
            resolve(res)
          } else {
            reject(res)
          }
        } else {
          reject(res)
        }
      }).catch(error => {
        reject(error)
      })
    })
  },
  /**
   * 获取 招生活动 数据
   * @param params
   */
  getRecruitData(params) {
    // debugger
    return new Promise((resolve, reject) => {
      const url = constant.ctx + 'manage/api/v1/ysrecruitactivity/query/param'
      request.get(url, params).then((res) => {
        if (res.code === 0) {
          resolve(res.result)
        }
      }).catch(e => {
        // todo 错误日志捕捉
        reject(e)
      })
    })
  },
  /**
   * 获取 招生活动 数据
   * @param params
   */
  getRecruitDetail(params, url) {
    // debugger
    return new Promise((resolve, reject) => {
      url = url || 'manage/api/v1/ysrecruitactivity/query/param'
      const url1 = constant.ctx + url
      request.get(url1, params).then((res) => {
        if (res.code === 0) {
          resolve(res.result)
        }
      }).catch(e => {
        // todo 错误日志捕捉
        reject(e)
      })
    })
  }
}

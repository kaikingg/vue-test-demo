import request from '@/utils/open/request'
import common from '@/utils/open/common'
import constant from '@/constant'

/**
 * 系统请求工具类，
 * 注意：API接口只做请求数据和格式化数据的事情，其他事务一概不处理
 */
export default {
  /**
   * 接口例子模板
   *
   * @param param
   * @param resolve
   * @param reject
   */
  demo(param, resolve, reject) {
    const url = common.static.ctx + '///'
    request.get(url, param).then(res => {
      if (res) {
        if (res.code === 0) {
          resolve(res)
        } else {
          reject(res)
        }
      } else {
        reject(res)
      }
    }).catch(error => {
      reject(error)
    })
  },
  /**
   * 发送短信验证码
   *
   *  const params = {
        phone: this.regForm.phone, // 用户填写的手机号
        code: this.regForm.code, // 随机编码
        unitId: '0' // 单位id
      }
   * @param param
   * @param resolve
   * @param reject
   */
  sendSms(param, resolve, reject) {
    const url = common.static.ctx + 'basics/api/v2/opensms/send/sms'
    request.get(url, param).then(res => {
      if (res) {
        if (res.code === 0) {
          resolve(res)
        } else {
          reject(res)
        }
      } else {
        reject(res)
      }
    }).catch(error => {
      reject(error)
    })
  },
  /**
   * 字典数据查询
   * @param dictType
   * @param {String} isOrderBy
   */
  getDicData(dictType, type, isOrderBy) {
    return new Promise((resolve, reject) => {
      // debugger
      if (!dictType) {
        reject()
      } else {
        const params = {
          dictType: dictType,
          isOrderBy: isOrderBy || '2', // isOrderBy 是否按字典项名称首字母拼音排序 1 是 2 否
          type: type || 1419 // 类型 1 系统 2 业务
        }
        const url = common.static.ctx + 'basics/api/v1/opendictinfo/query/dict/list'
        // debugger
        request.get(url, params).then((res) => {
          if (res.code === 0) {
            resolve(res)
          } else {
            reject(res)
          }
        }).catch(e => {
          reject(e)
        })
      }
    })
  },
  /**
   * 获取首页banner图
   */
  getBannerList() {
    return new Promise((resolve, reject) => {
      const url = constant.ctx + 'manage/api/v1/lzdbanner/query/list'
      request.get(url, {}).then((res) => {
        // debugger
        if (res.code === 0) {
          resolve(res)
        } else {
          reject()
        }
      }).catch(e => {
        reject(e)
      })
    })
  }

}

/**
 * 在线报名
 * recruit
 */

import request from '@/utils/open/request'
import constant from '@/constant'
// import common from '@/utils/open/common'

export default {
  /**
   * 获取学生报名记录详情
   * @param param
   * @param url
   */
  getRecruitStudentDetailData(item) {
    // debugger
    return new Promise((resolve, reject) => {
      // debugger
      item.paramUrl = item.paramUrl || 'ysrecruitstudent/query/one'
      const params = {
        id: item.archivesId // 档案主键id
      }
      const url = constant.ctx + 'manage/api/v1/' + item.paramUrl
      request.get(url, params).then(res => {
        if (res) {
          if (res.code === 0) {
            resolve(res)
          } else {
            reject(res)
          }
        } else {
          reject(res)
        }
      }).catch(error => {
        reject(error)
      })
    })
  }
}

import request from '@/utils/open/request'
import common from '@/utils/open/common'
import constant from '@/constant'

/**
 * 系统请求工具类，
 * 注意：API接口只做请求数据和格式化数据的事情，其他事务一概不处理
 */
export default {
  /**
   * 接口例子模板
   *
   * @param param:{
   *
   * }
   * @param resolve
   * @param reject
   */
  demo(param, resolve, reject) {
    const url = common.static.ctx + '///'
    request.get(url, param).then(res => {
      if (res) {
        if (res.code === 0) {
          resolve(res)
        } else {
          reject(res)
        }
      } else {
        reject(res)
      }
    }).catch(error => {
      reject(error)
    })
  },
  /**
   * 统一下单接口
   * @param param:{
   * appid            （是）公众账号ID
   * attach            (否)附加数据，在查询API和支付通知中原样返回，可作为自定义参数使用。
   * body             （是）商品简单描述，该字段请按照规范传递，具体请见参数规定
   * out_trade_no     （是）商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*@ ，且在同一个商户号下唯一。详见商户订单号
   * total_fee        （是）订单总金额，单位为分，详见支付金额
   * openid           （是）用户id
   * spbill_create_ip （是）APP和网页支付提交用户端ip，Native支付填调用微信支付API的机器IP。
   * notify_url       （是）异步接收微信支付结果通知的回调地址，通知url必须为外网可访问的url，不能携带参数。
   * }
   * @param resolve
   * @param reject
   */
  wechatUnifiedorder(param, resolve, reject) {
    const url = common.static.ctx + 'pay/api/v1/wechat/unifiedorder'
    request.postForm(url, param).then(res => {
      if (res) {
        if (res.code === 0) {
          resolve(res)
        } else {
          reject(res)
        }
      } else {
        reject(res)
      }
    }).catch(error => {
      reject(error)
    })
  },
  /**
   * 查询订单状态数据接口
   * @param param {
   *     appid:'',
   *     outTradeNo:''
   * }
   * @param resolve
   * @param reject
   */
  wechatOrderquery(param, resolve, reject) {
    const url = common.static.ctx + 'pay/api/v1/wechat/orderquery'
    request.get(url, param).then(res => {
      if (res) {
        if (res.code === 0) {
          resolve(res)
        } else {
          reject(res)
        }
      } else {
        reject(res)
      }
    }).catch(error => {
      reject(error)
    })
  },
  /**
   * 关闭订单数据接口
   * @param param {
   *     appid:'',
   *     outTradeNo:''
   * }
   * @param resolve
   * @param reject
   */
  wechatCloseorder(param, resolve, reject) {
    const url = common.static.ctx + 'pay/api/v1/wechat/closeorder'
    request.get(url, param).then(res => {
      if (res) {
        if (res.code === 0) {
          resolve(res)
        } else {
          reject(res)
        }
      } else {
        reject(res)
      }
    }).catch(error => {
      reject(error)
    })
  }
}

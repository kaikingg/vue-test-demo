import request from '@/utils/open/request'
import common from '@/utils/open/common'

/**
 * 数字字典请求工具类
 * 注意：API接口只做请求数据和格式化数据的事情，其他事务一概不处理
 */
export default {
  /**
   * 接口例子模板
   *
   * @param param
   * @param resolve
   * @param reject
   */
  demo(param, resolve, reject) {
    const url = common.static.ctx + '///'
    request.get(url, param).then(res => {
      if (res) {
        if (res.code === 0) {
          resolve(res)
        } else {
          reject(res)
        }
      } else {
        reject(res)
      }
    }).catch(error => {
      reject(error)
    })
  },
  /**
   * 获取字段
   * @param key
   * @param resolve
   */
  getDicInfo(params, resolve) {
    const url = common.static.ctx + 'basics/api/v1/opendictinfo/query/dict/list'
    request.get(url, params).then(res => {
      if (res) {
        if (res.code === 0) {
          // console.log(res)
          const result = []
          if (res.result.length > 0) {
            for (const k of res.result) {
              result.push({
                key: '' + k.dictinfoValue,
                value: '' + k.dictinfoKey
              })
            }
          }
          resolve(result)
        } else {
          Promise.reject(res)
        }
      } else {
        Promise.reject(res)
      }
    }).catch(error => {
      Promise.reject(error)
    })
  },
  /**
   * 获取岗位数据字典
   */
  getPostDic(unitId, fn) {
    request.get(common.static.ctx + 'manage' + '/api/v1/yspost/query/list', { unitId: unitId }).then(res => {
      if (res) {
        const result = res.result
        const array = []
        for (const k of result) {
          array.push({
            key: k.id,
            value: k.postName
          })
        }
        fn(array)
      }
    }).catch(res => {
    })
  },
  /**
   * 获取字典信息
   */
  getUnitDic(fn) {
    request.get(common.static.ctx + 'auth' + '/api/v1/pc/syscenterunit/query/cache', {}).then(res => {
      if (res) {
        const result = res.result
        const array = []
        for (const key in result) {
          array.push({
            key: key,
            value: result[key]
          })
        }
        fn(array)
      }
    }).catch(res => {
    })
  },
  /**
   * 获取班级信息
   */
  getClassDic(unitId, fn) {
    request.get(common.static.ctx + 'record' + '/api/v1/ysclass/query/list', { unitId: unitId }).then(res => {
      if (res) {
        const result = res.result
        const array = []
        for (const k of result) {
          array.push({
            key: k.id,
            value: k.className
          })
        }
        fn(array)
      }
    }).catch(res => {
    })
  },
  /**
   * 获取服务信息
   */
  getServiceDic(fn) {
    request.get(common.static.ctx + 'basics' + '/api/v1/discovery/service', {}).then(res => {
      if (res) {
        const result = res.result
        const array = []
        for (const key in result) {
          array.push({
            key: result[key],
            value: result[key]
          })
        }
        fn(array)
      }
    }).catch(res => {
    })
  }
}

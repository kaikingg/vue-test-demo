export default {
  editData: {
    modifymode: 2,
    canAdd: 0,
    saveApiCode: 0,
    title: '教育情况子表',
    groups: [
      {
        editAction: 2,
        groupLabel: '测试数据',
        keyId: 'B3A0D4F6CC55CA2A',
        list: [
          {
            colName: 'WORKUNIT',
            colNull: 1,
            colType: 'date',
            colWidth: 40,
            show: false, // 显示弹窗 --------- 特有
            dateType: 'range', // 日期区间选择 空：表示单个日期选择 range：区间选择 --------- 特有
            dropList: null,
            editFormat: null,
            inputType: 1,
            isLinkField: 0,
            label: '日期选择',
            note: null,
            precision: 0,
            text: null,
            undermsg: null,
            unit: '',
            value: ''
          },
          {
            colName: 'WORKUNIT',
            colNull: 1,
            colType: 'varchar',
            colWidth: 40,
            dropList: null,
            editFormat: null,
            inputType: 1,
            isLinkField: 0,
            label: '单行输入框',
            note: null,
            precision: 0,
            text: null,
            undermsg: null,
            unit: '',
            value: '南京伺'
          },
          {
            colName: 'WORKUNIT',
            colNull: 1,
            colType: 'text',
            colWidth: 40,
            dropList: null,
            editFormat: null,
            inputType: 1,
            isLinkField: 0,
            label: '多行输入框',
            maxlength: 50, // --------- 特有
            showWordLimit: true, //  --------- 特有
            placeholder: '请输入留言',
            note: null,
            precision: 0,
            text: null,
            undermsg: null,
            unit: '',
            value: ''
          },
          {
            colName: 'ZW_NAME',
            colNull: 1,
            colType: 'varchar',
            colWidth: 40,
            dropList: null,
            editFormat: null,
            inputType: 0,
            isLinkField: 0,
            label: '上家工作职务',
            note: null,
            precision: 0,
            text: null,
            undermsg: null,
            unit: '',
            value: '员工'
          },
          {
            colName: 'EDUCATION',
            colNull: 1,
            colType: 'int',
            colWidth: 30,
            show: false, // 显示弹窗 --------- 特有
            isShowToolbar: false, // 是否显示顶部表格 --------- 特有
            toolbarTitle: '顶部标题', // 顶部标题 --------- 特有
            toolbarPosition: 'top', // 顶部标题位置，可选bottom,top --------- 特有
            defaultIndex: 0, // 默认选中，单列特有 --------- 特有
            dropList: [
              {
                text: '小学', //  --------- vant 使用text
                value: '1'
              },
              {
                text: '初中', //  --------- vant 使用text
                value: '2'
              },
              {
                text: '高中', //  --------- vant 使用text
                value: '3'
              }
            ],
            editFormat: null,
            inputType: 2,
            isLinkField: 0,
            label: '下拉框(1列)',
            note: null,
            precision: 0,
            text: '职业高级中学',
            undermsg: null,
            unit: '',
            value: '4'
          },
          {
            colName: 'EDUCATION1',
            colNull: 1,
            colType: 'int',
            colWidth: 30,
            show: false, // 显示弹窗 --------- 特有
            isShowToolbar: true, // 是否显示顶部 --------- 特有
            toolbarTitle: '顶部标题', // 顶部标题 --------- 特有
            toolbarPosition: 'bottom', // 顶部标题位置，可选bottom,top --------- 特有
            dropList: [
              // 第一列
              {
                values: ['周一', '周二', '周三', '周四', '周五'], //  --------- vant 特有
                defaultIndex: 2 //  --------- vant 特有
              },
              // 第二列
              {
                values: ['上午', '下午', '晚上'],
                defaultIndex: 1
              }
            ],
            editFormat: null,
            inputType: 2,
            isLinkField: 0,
            label: '下拉框(2列)',
            note: null,
            precision: 0,
            text: '职业高级中学',
            undermsg: null,
            unit: '',
            value: '4'
          }
        ]
      }
    ]
  }
}

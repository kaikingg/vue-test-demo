import dic from './modules/dic'
import kg from './modules/kg'
import sys from './modules/sys'
import user from './modules/user'
import common from './modules/common'
import pay from './modules/pay'
import recruit from './modules/recruit'

const api = {
  dic: dic,
  kg: kg,
  sys: sys,
  user: user,
  common: common,
  recruit: recruit,
  pay: pay
}
export default api

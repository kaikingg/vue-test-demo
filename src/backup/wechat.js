import wxAPI from '@/utils/open/wechat-plugin'
import common from '@/utils/open/common'

// common.cutOutUrl() // 截取外链参数 确保要在登录后能正常截取到末尾自带参数，若在未登录就跳转微信推送进来，可能会出现直接跳转到登录页面后，再截取的可能

window.onload = function() {
  // console.log('页面加载完成，注册微信验证')
  // 判断是否是微信坏境
  const isWechat = common.isWechatClient()
  if (isWechat) {
    common.setKeyVal('sys', 'isWechat', isWechat)
    wxAPI.config() // 微信注入权限验证配置
  }
}

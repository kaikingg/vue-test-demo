import Vue from 'vue'
// 全局注册指令
const number = {
  // bind: function(el, binding) {
  //   const inputArea = el.querySelector('input')
  //   el.onkeydown = function(event) {
  //     return event.keyCode !== 229 && (event.keyCode >= 48 && event.keyCode <= 57 || event.keyCode >= 96 && event.keyCode <= 105 || event.keyCode === 8) && event.keyCode !== 13
  //   }
  //   el.addEventListener('keydown', el.onkeydown)
  // },
  // unbind: function(el) {
  //   el.removeEventListener('keydown', el.onkeydown)
  // }
}

const clickOutSide = {
  bind: function(el, binding, vnode) {
    function documentHandler(e) {
      if (el.contains(e.target)) {
        return false
      } else {
        if (binding.expression) {
          // binding.value(e)
          console.log(binding, binding.value)
          // alert('******************')
          binding.value()
          // binding.value.this.isFocus = false
        }
      }
    }

    el.__vueClickOutside__ = documentHandler
    document.addEventListener('click', documentHandler)
  },
  update: function() {

  },
  unbind: function(el, binding) {
    document.removeEventListener('click', el.__vueClickOutside__)
    delete el.__vueClickOutside__
  }
}
export default { number, clickOutSide }

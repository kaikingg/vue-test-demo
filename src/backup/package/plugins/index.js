// plugin里存放插件，插件命名规则：XXX-plugin
import touch from './modules/touch-plugin'
// import VueScroller from 'vue-scroller'
import VueLazyLoad from 'vue-lazyload'
import VueAwesomeSwiper from 'vue-awesome-swiper'
// import VueHtml5Editor from 'vue-html5-editor'
// import options from './modules/VueHtml5Editor-options'

// import fastClick from 'fastclick' // 屏蔽移动端点击事件，延迟300毫秒
// fastClick.attach(document.body)

export default {
  touch,
  // VueScroller,
  VueAwesomeSwiper,
  VueLazyLoad: { name: VueLazyLoad, options: {
    // error: '/static/img/parent/icon-lazy-loading.png',
    error: '/static/img/icon-lazy-loading.png',
    // loading: '/static/loading-svg/loading-spinning-bubbles.svg',
    loading: '/static/img/icon-lazy-loading.png'
  }}
  // VueHtml5Editor: {
  //   name: VueHtml5Editor, options: options
  // }
}

import $constant from '@/constant/index'
import axios from 'axios'
const toolbarOptions = [
  ['bold', 'italic'], // toggled buttons
  ['blockquote', 'code-block'],
  //
  [{ 'header': 1 }, { 'header': 2 }], // custom button values
  [{ 'list': 'ordered' }, { 'list': 'bullet' }],
  [{ 'script': 'sub' }, { 'script': 'super' }], // superscript/subscript
  [{ 'indent': '-1' }, { 'indent': '+1' }], // outdent/indent
  // [{ 'direction': 'rtl' }], // text direction
  //
  // [{ 'size': ['small', false, 'large', 'huge'] }], // custom dropdown
  // [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
  //
  [{ 'color': [] }, { 'background': [] }], // dropdown with defaults from theme
  // [{ 'font': [] }],
  [{ 'align': [] }],
  ['image'],
  ['clean'] // remove formatting button
]

/* 富文本编辑图片上传配置*/
const uploadConfig = {
  action: $constant.ctx + 'zuul/basics/api/v1/attachment/upload', // 必填参数 图片上传地址
  methods: 'POST', // 必填参数 图片上传方式
  token: sessionStorage.getItem('token'), // 可选参数 如果需要token验证，假设你的token有存放在sessionStorage
  name: 'file', // 必填参数 文件的参数名
  size: 500, // 可选参数   图片大小，单位为Kb, 1M = 1024Kb
  accept: 'image/png, image/gif, image/jpeg, image/bmp, image/x-icon' // 可选 可上传的图片格式
}
const options = {
  placeholder: '',
  theme: 'snow', // or 'bubble'
  modules: {
    toolbar: {
      container: toolbarOptions, // 工具栏
      handlers: {
        'image': function(value) {
          if (value) {
            const self = this
            var fileInput = this.container.querySelector('input.ql-image[type=file]')
            if (fileInput === null) {
              fileInput = document.createElement('input')
              fileInput.setAttribute('type', 'file')
              // 设置图片参数名
              if (uploadConfig.name) {
                fileInput.setAttribute('name', uploadConfig.name)
              }
              // 可设置上传图片的格式
              fileInput.setAttribute('accept', uploadConfig.accept)
              fileInput.classList.add('ql-image')
              // 监听选择文件
              fileInput.addEventListener('change', function() {
                // 创建formDatal
                const formData = new FormData()
                formData.append(uploadConfig.name, fileInput.files[0])
                const config = {
                  headers: { 'Content-Type': 'multipart/form-data' }
                }
                axios.post(uploadConfig.action, formData, config).then((res) => {
                  console.log(res, 'res')
                  const length = self.quill.getSelection(true).index
                  self.quill.insertEmbed(length, 'image', $constant.file_ctx + res.result[0].dir)
                  console.log(length, 'length')
                  self.quill.setSelection(length + 1)
                })
              })
              this.container.appendChild(fileInput)
            }
            fileInput.click()
          } else {
            this.quill.format('image', false)
          }
        }
      }
    }
  }
}
export default options

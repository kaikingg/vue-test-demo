import moment from 'moment'
const dateFormat = function(dataStr, pattern = 'YYYY-MM-DD HH:mm:ss') {
  return moment(dataStr).format(pattern)
}

export default {
  dateFormat
}

/**
 * 权限设置
 * 路由跳转拦截
 */
import router from './router'
import store from './store'
import common from '@/utils/open/common'
import validate from '@/utils/open/validate'
import websocket from '@/utils/open/websocket-plugin'
import ext from '@/ext'

let urlParam = common.getKeyVal('user', 'firstUrlParam', 'sessionStorage') || {}
if (validate.isNull(urlParam)) { // 考虑到链接传参中的角色id问题，保证每次打开浏览器器，只截取一次，否则刷新后可能会出现当前选中的角色被修改问题
  common.cutOutUrl() // 截取外链参数 确保要在登录后能正常截取到末尾自带参数，若在未登录时，从微信推送进来，可能会出现直接跳转到登录页面后，再截取的可能
  urlParam = common.getKeyVal('user', 'firstUrlParam', 'sessionStorage') || {}
  // console.log('------- 1.截取链接带参 --------', urlParam)
} else {
  // console.log('------- 2.使用缓存链接带参 --------', urlParam)
}

/**
 * 白名单配置
 * @type {string[]}
 */
const whiteList = [
  'Home',
  'Login',
  'ChangePwd',
  'Register',
  'OfficalNet',
  'PayDemo',
  'guardianSelectActive',
  'guardianSelectedActivityDetail',
  'Redirect'
]

/**
 * 路由公用变量申明
 */
// eslint-disable-next-line
let isPush = false
const methods = ['push', 'go', 'replace', 'forward', 'back']
common.setCache('tokenUuid', common.uuid())
methods.forEach(key => {
  const method = router[key].bind(router)
  router[key] = function(...args) {
    isPush = true
    method.apply(null, args)
  }
})
/**
 * 前置处理
 */
router.beforeEach((to, from, next) => {
  // console.log('--------------permission-location: ', location)
  // const mainUrl = location.protocol + location.host + '/'
  // console.log('--------------permission-mainUrl: ', mainUrl)
  // debugger
  // 保存需要缓存的路由
  // if (to.meta.keepAlive) {
  //   const list = store.state.user.keepAliveList
  //   if (!common.oneOf(to.name, list)) {
  //     // debugger
  //     store.dispatch('user/UpdateKeepAliveList', list)
  //   }
  // }
  // setTimeout(() => {
  // 统计代码
  const _hmt = _hmt || []
  window._hmt = _hmt // 必须把_hmt挂载到window下，否则找不到
  if (to.path) window._hmt.push(['_trackPageview', '/#' + to.fullPath])
  // store.dispatch('sys/UpdateIsLoading', true)
  if (!to.name) { // 避免 前段路由修改，而导致后端 推送的连接无法正常访问
    if (to.name !== 'NotData') {
      router.push({ name: 'NotData' })
    } else {
      router.push({ name: 'Home' })
    }
    return
  }
  outLink(to)
  mainHandler(store, to, from, next)
  // }, 2000)
})
/**
 * 后置处理
 */
router.afterEach((to) => {
  switchRouterHeaderAfter(store, to)
})

/**
 * 处理底部导航状态设置
 * @param isLogin
 * @param store
 * @param to
 * @param from
 * @param next
 */
// function tabbarStatusHandler(store, to, from, next) {
//   const isLogin = store.state.user.isLogin
//   if (isLogin) {
//     if (!validate.isNull(to.query.tabIndex)) {
//       // store.dispatch('tabbar/UpdateSelectTabIndex', to.query.tabIndex)
//     } else {
//       // 在退出登录重新登录路由到主页时更新selectTab
//       // store.dispatch('tabbar/UpdateSelectTabIndex', 0)
//     }
//   } else {
//     // debugger
//     if (to.name === 'Home') {
//       // store.dispatch('tabbar/UpdateSelectTabIndex', 0)
//     }
//   }
//   // store.dispatch('tabbar/UpdateIsShow', (to.meta.index === 1 || to.meta.index === 0))
// }

/**
 * 处理主逻辑
 * @param store
 * @param to
 * @param from
 * @param next
 */
function mainHandler(store, to, from, next) {
  const isLogin = store.state.user.isLogin
  if (isLogin) {
    if (to.name === 'Login') { // 若是已经登录，则直接返回首页
      common.goHome(true) // 返回首页
      return
    }
    nextHandler(to, from, next)
  } else { // 自动登录
    const userAccount = common.getLocalCache('userAccount')
    // 若用户信息不为空，则往下走
    if (!validate.isNull(userAccount)) {
      // debugger
      store.dispatch('user/Login', userAccount).then(result => {
        // debugger
        if (result.loginStatus) {
          // debugger
          ext.user.timingChangeToken() // 每27分钟刷新token
          // 登录后请求平台信息数据（获取包含信息的对象和未读信息总数）
          store.dispatch('user/UpdateUnreadList', 0)
          store.dispatch('user/getCodeUserInfo')
          schoolOfBusiness(store, to, from, next)
        } else {
          touristMode(store, next)
        }
      }).catch(() => {
        touristMode(store, next)
      })
    } else { //
      const tokenVal = common.getToken()
      if (!tokenVal) {
        // debugger
        store.dispatch('user/GetToken').then(token => {
          // debugger
          ext.user.timingChangeToken() // 每27分钟刷新token
          whiteHandler(to, from, next)
        }).catch(() => {
          // touristMode(store, next)
          // 不能继续跳 touristMode ，否则会进入死循环
        })
      } else {
        // debugger
        whiteHandler(to, from, next)
      }
    }
  }
}

/**
 * 校园业务
 * @param store
 * @param to
 * @param from
 * @param next
 */
function schoolOfBusiness(store, to, from, next) {
  // 获取单位信息
  store.dispatch('user/GetUnitList').then(result => {
    if (result.result.length !== 0) {
      // 获取当前单位信息
      store.dispatch('user/ChangeUnit').then(unitData => {
        console.log('unitData:', unitData)
        // 获取微信信息(每次切换单位都需要重新获取)，需要通过unit数据判断
        store.dispatch('user/GetWechatInfo').then(wechatInfo => {
          // console.log('获取微信信息', res)
        }).catch(e => {
          // console.log(e)
        })

        // 获取当前单位用户信息
        store.dispatch('user/GetUserInfo').then(userInfo => {
          if (userInfo.data.length !== 0) {
            // websocket启动
            websocket.wsInitAck()
            // 获取剩余未读信息
            store.dispatch('user/UpdateUnreadList', 1)
            // 获取权限数据并处理
            store.dispatch('user/GetPermission', userInfo).then(permissionData => {
              nextHandler(to, from, next)
            }).catch(e => {
              // 报错处理
            })
          } else {
            nextHandler(to, from, next)
          }
        }).catch(() => {
          // 游客模式
          touristMode(store, next)
        })
      }).catch(() => {
        touristMode(store, next)
      })
    } else {
      // 获取当前单位用户信息
      store.dispatch('user/GetUserInfo').then(userInfo => {
        nextHandler(to, from, next)
      })
      nextHandler(to, from, next)
    }
  }).catch(() => {
    touristMode(store, next)
  })
}

/**
 * 白名单跳转处理
 * @param to
 * @param from
 * @param next
 */
function whiteHandler(to, from, next) {
  if (whiteList.indexOf(to.name) !== -1) {
    nextHandler(to, from, next)
  } else {
    next({ name: 'Login', query: { 'redirect': encodeURI(to.fullPath) }})
  }
}

/**
 * 跳转回到原始路由操作
 */
function nextHandler(to, from, next) {
  // tabbarStatusHandler(store, to, from, next)
  if (to.name !== 'Home') {
    next()
  } else {
    if (from.name === 'Home' && to.name !== 'Home') {
      next({ name: to.name })
    } else {
      next()
    }
  }
  // 如果在页面已自定义了右键方法则不再通过路由配置
  store.dispatch('navigation/UpdateHeaderObj', to.meta)
}

/**
 * 跳转外链地址
 * @param to
 */
function outLink(to) {
  if (/\/http/.test(to.name)) {
    const url = to.name.split('http')[1]
    window.location.href = `http${url}`
    return false
  }
}
/**
 * 游客模式
 */
function touristMode(store, next) {
  jumpInvalid500()
}

/**
 * 登录过程中出现错误，跳转到该页面
 */
function jumpInvalid500() {
  router.replace({ name: 'Invalid500' })
}
/**
 * 顶部导航切换前进返回 后置事件
 * @param store
 * @param to
 * @param from
 * @param next
 */
function switchRouterHeaderAfter(store, to) {
  isPush = false
  // store.dispatch('sys/UpdateIsLoading', false)
}


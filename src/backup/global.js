import Vue from 'vue'
import utils from '@/utils' // 公共方法
import directive from '@/package/directive/index' // 指令
import plugins from '@/package/plugins' // 插件
import filters from '@/package/filter' // 过滤器
// import headImage from './components/HeadImageItem/index.vue'
// import Page from '@/components/Frame/Page'
import VueStar from 'vue-star'
// import Scroller from '@/components/Scroller'
import vConsole from 'vconsole'
import myMixin from './mixin'

// import VueTour from 'vue-tour' // Vue Tour 引导
// require('vue-tour/dist/vue-tour.css')
// Vue.use(VueTour)

// 全局注册默认头像组件
Vue.component('headImage', headImage)
Vue.component('Page', Page)
Vue.component('VueStar', VueStar)
// Vue.component('Scroller', Scroller)
// 全局注册过滤器
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})
// 全局注册指令
Object.keys(directive).forEach(key => {
  Vue.directive(key, directive[key])
})
// 全局使用插件
Object.keys(plugins).forEach(key => {
  // 如果插件需配置设置，则注册插件需添加options
  if (plugins[key].name) {
    Vue.use(plugins[key].name, plugins[key].options)
  } else {
    Vue.use(plugins[key])
  }
})
// 全局使用mixin
Vue.mixin(myMixin.myMixin)
// 全局设置prototype
Object.keys(utils).forEach(key => {
  Vue.prototype[key] = utils[key]
})

if (process.env.ENV_CONFIG === 'prod') { // 生产环境
  // console.log('---- 生产环境 ----')
  // Vue.prototype.$vConsole = new vConsole()
} else if (process.env.ENV_CONFIG === 'sit') { // 测试环境
  // console.log('---- 测试环境 ----')
  // eslint-disable-next-line new-cap
  Vue.prototype.$vConsole = new vConsole()
} else { // 开发环境
  // console.log('---- 开发环境 ----')
}
